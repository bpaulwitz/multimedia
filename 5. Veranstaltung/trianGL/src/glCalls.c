#include "glCalls.h"

void glCheck(const char* errorText)
{
	GLenum error = glGetError();

	if(error != GL_NO_ERROR){
		printf("GLError: %s - %d\n", errorText, error);
		exit(EXIT_FAILURE);
	}
}

char* readShaderFromFile(char* filename) 
{
	FILE *fp;
	long lSize;
	char *buffer;

	fp = fopen ( filename , "rb" );
	assert(fp);

	fseek( fp , 0L , SEEK_END);
	lSize = ftell( fp );
	rewind( fp );

	/* allocate memory for entire content */
	buffer = calloc( 1, lSize + 1 );
	if ( !buffer ) fclose(fp), fputs("memory alloc fails", stderr), exit(1);

	/* copy the file into the buffer */
	if ( 1 != fread( buffer , lSize, 1 , fp) )
		fclose(fp), free(buffer), fputs("entire read fails", stderr), exit(1);

	return buffer;
}

GLuint compileShader(GLenum type, const char* shaderSource, char* shaderTag)
{
	// shader reference
	GLuint shader;

	// create an empty shader object
	shader = glCreateShader(type);
	glCheck("glCreateShader");

	// put source code into empty shader object
	glShaderSource(shader, 1, &shaderSource, NULL);
	glCheck("glShaderSource");

	// compile the shader
	glCompileShader(shader);
	glCheck("glCompileShader");

	// compile status
	GLint compiled;

	// check compile status
	glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);
	glCheck("glGetShaderiv GL_COMPILE_STATUS");

	if(!compiled)
	{
		GLint infoLen = 0;

		// get length of error message
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLen);
		glCheck("glGetShaderiv GL_INFO_LOG_LENGTH");

		if(infoLen > 1)
		{
			char* errorMessage = malloc(sizeof(char*)*infoLen);

			// get error message 
			glGetShaderInfoLog(shader, infoLen, NULL, errorMessage);

			printf("Error compiling shader (%s): %s\n", shaderTag, errorMessage);

			free(errorMessage);
		}

		// delete shader object
		glDeleteShader(shader);
		glCheck("glDeleteShader");

		return 0;
	}

	return shader;
}

GLboolean init(GLFWwindow* window, int sizeData)
{
	// allocate memory for user data struct
	UserData* userData = malloc(sizeof(UserData));
	glfwSetWindowUserPointer(window, (void*)userData);

	// create vertex shader
	char* vertexShaderSource = readShaderFromFile("shader/vertex.glsl");
	GLuint vertexShader = compileShader(GL_VERTEX_SHADER, vertexShaderSource, "Vertex Shader");
	free(vertexShaderSource);	

	// create fragment shader
	char* fragmentShaderSource = readShaderFromFile("shader/fragment.glsl");
	GLuint fragmentShader = compileShader(GL_FRAGMENT_SHADER, fragmentShaderSource, "Fragment Shader");
	free(fragmentShaderSource);	

	// hint to release the shader compiler -> for GPU
	glReleaseShaderCompiler();
	glCheck("glReleaseShaderCompiler");

	if(vertexShader == 0 || fragmentShader == 0)
	{
		return GL_FALSE;
	}

	// program reference
	GLuint programObject;

	// create empty program object
	programObject = glCreateProgram();
	glCheck("glCreateProgram");

	// attach shaders to program object
	glAttachShader(programObject, vertexShader);
	glCheck("glAttachShader, vertexShader");

	glAttachShader(programObject, fragmentShader);
	glCheck("glAttachShader, fragmentShader");

	// link shaders
	glLinkProgram(programObject);
	glCheck("glLinkProgram");

	// detach shaders from program object
	glDetachShader(programObject, vertexShader);
	glCheck("glDetachShader, vertexShader");

	glDetachShader(programObject, fragmentShader);
	glCheck("glDetachShader, fragmentShader");

	// delete shader objects
	glDeleteShader(vertexShader);
	glCheck("glDeleteShader vertexShader");

	glDeleteShader(fragmentShader);
	glCheck("glDeleteShader fragmentShader");

	// link status
	GLint linked;

	// check linker status
	glGetProgramiv(programObject, GL_LINK_STATUS, &linked);
	glCheck("glGetProgramiv, GL_LINK_STATUS");

	if(!linked)
	{
		GLint infoLen;

		// get length of error message
		glGetProgramiv(programObject, GL_INFO_LOG_LENGTH, &infoLen);

		if(infoLen > 1)
		{
			char* errorMessage = malloc(sizeof(char*)*infoLen);

			// get error message 
			glGetProgramInfoLog(programObject, infoLen, NULL, errorMessage);

			printf("Error linking program: %s\n", errorMessage);

			free(errorMessage);
		}

		glDeleteProgram(programObject);
		glCheck("glDeleteProgram");

		return GL_FALSE;
	}

	// use program
	glUseProgram(programObject);
	glCheck("glUseProgram");

	// store program object
	userData->programObject = programObject;

	// create and bind VAO (vertex array object)
	glGenVertexArrays(1, &(userData->vertexArrayObject));
	glCheck("glGenVertexArrays");

	glBindVertexArray(userData->vertexArrayObject);
	glCheck("glBindVertexArray");

	// create VBO (vertex buffer object)
	glGenBuffers(1, &(userData->vertexBufferObject));
	glCheck("glGenBuffers");

	glBindBuffer(GL_ARRAY_BUFFER, userData->vertexBufferObject);
	glCheck("glBindBuffer");
	
	// attribute index, component count, data type, normalize, 
	glVertexAttribPointer(ATTRIB_POSITION, 3, GL_FLOAT, GL_FALSE, 
		// stride, pointer to offset
		sizeof(VertexData), (GLvoid*) offsetof(VertexData, position));
	glCheck("glVertexAttribPointer ATTRIB_POSITION");

	// enable vertex attribute
	glEnableVertexAttribArray(ATTRIB_POSITION);
	glCheck("glEnableVertexAttribArray ATTRIB_POSITION");

	// attribute index, component count, data type, normalize, 
	glVertexAttribPointer(ATTRIB_COLOR, 3, GL_FLOAT, GL_FALSE, 
		// stride, pointer to offset
		sizeof(VertexData), (GLvoid*) offsetof(VertexData, color));
	glCheck("glVertexAttribPointer ATTRIB_COLOR");

	// enable vertex attribute
	glEnableVertexAttribArray(ATTRIB_COLOR);
	glCheck("glEnableVertexAttribArray ATTRIB_COLOR");

	// get the window size
	int width, height;
	glfwGetFramebufferSize(window, &width, &height);
	glCheck("glfwGetFramebufferSize");

	// set viewport 
	glViewport(0, 0, width, height);
	glCheck("glViewport");

	// define clear color
	glClearColor(0.2f, 0.4f, 0.2f, 0.0f);
	glCheck("glClearColor");

	//initialize time and offset
	userData->time = 0.0f;
	userData->offset = 0.0f;

	return GL_TRUE;
}

void draw (GLFWwindow* window, int sizeData)
{
	UserData* userData = (UserData*) glfwGetWindowUserPointer(window);

	// spinning the wheel of time
	if(userData->time < 2 * M_PI)
	{
		
		userData->time += (2 * M_PI) / 300;	
	}
	else
	{
		userData->time = 0;
	}

	//generate points to draw
	VertexData* data = generatePoints(sizeData, userData->offset, userData->time);

	// upload data to GPU
	glBufferData(GL_ARRAY_BUFFER, sizeof(VertexData) * (sizeData + 2), (const GLvoid*) data, GL_STATIC_DRAW);
	glCheck("glBufferData");

	// clear framebuffer 
	glClear(GL_COLOR_BUFFER_BIT);
	glCheck("glClear");

	// draw triangle
	glDrawArrays(GL_TRIANGLE_FAN, 0, sizeData + 2);
	glCheck("glDrawArrays");

	// swap framebuffer
	glfwSwapBuffers(window);

	//free allocated data
	free(data);
}

void teardown(GLFWwindow* window)
{
	UserData* userData = (UserData*) glfwGetWindowUserPointer(window);	

	// delete VBO
	glDeleteBuffers(1, &(userData->vertexBufferObject));
	glCheck("glDeleteBuffers vertexBufferObject");

	// delete VBA
	glDeleteBuffers(1, &(userData->vertexArrayObject));
	glCheck("glDeleteBuffers vertexArrayObject");

	// delete program
	glDeleteProgram(userData->programObject);
	glCheck("glDeleteProgram");

	free(userData);
}

void error_callback(int error, const char* description)
{
    fprintf(stderr, "Error: %s\n", description);
}

//generate the VertexData array of a polygon with n edges, colorized in hsv at a certain time (for the rotation)
VertexData* generatePoints(int n, double offset, float time)
{
	//amount corners + middle + last one to connect with first corner
	VertexData* data = (VertexData*)malloc( (n + 2) * sizeof(VertexData));

	double rotateAlpha = (2 * M_PI) / (double)n;
	double hueAlpha = 1.0f / (double)n;

	//point in the middle -> white
	data[0].position[0] = 0.0f;
	data[0].position[1] = 0.0f;
	data[0].position[2] = 0.0f;

	data[0].color[0] = 0.0f;
	data[0].color[1] = 0.0f;
	data[0].color[2] = 1.0f;

	//first edge at [1, 0, 0]
	data[1].position[0] = 1.0f;
	data[1].position[1] = 0.0f;
	data[1].position[2] = 0.0f;

	data[1].color[0] = offset;
	data[1].color[1] = 1.0f;
	data[1].color[2] = 1.0f;

	//each next edge is the last edge rotate by rotateAlpha at the z-Axis
	for (int i = 2; i <= n; ++i)
	{
		//"rotating" the hue by hueAlpha
		double hue = data[i-1].color[0] + hueAlpha;
		clampOffset(&hue);

		data[i].position[0] = cos(rotateAlpha) * data[i-1].position[0] - sin(rotateAlpha) * data[i-1].position[1];
		data[i].position[1] = sin(rotateAlpha) * data[i-1].position[0] + cos(rotateAlpha) * data[i-1].position[1];
		data[i].position[2] = 0.0f;

		data[i].color[0] = hue;
		data[i].color[1] = 1.0f;
		data[i].color[2] = 1.0f;
	}

	//last edge has to be at the same position and color as the first edge to complete the polygon
	data[n+1].position[0] = 1.0f;
	data[n+1].position[1] = 0.0f;
	data[n+1].position[2] = 0.0f;

	data[n+1].color[0] = offset;
	data[n+1].color[1] = 1.0f;
	data[n+1].color[2] = 1.0f;

	//rotation with time
	//for (int i = 1; i <= n + 1; ++i)
	//{
	//	data[i].position[0] = cos(time) * data[i].position[0] + sin(time) * data[i].position[2];
	//	data[i].position[1] = data[i].position[1];
	//	data[i].position[2] = -sin(time) * data[i].position[0] + cos(time) * data[i].position[2];
	//}

	return data;
}

//changes offset respectively to the imput of the arrow keys
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	UserData* userData = (UserData*) glfwGetWindowUserPointer(window);

    if ((key == GLFW_KEY_UP && action == GLFW_PRESS) || (key == GLFW_KEY_UP && action == GLFW_REPEAT))
    {
    	userData->offset += 0.005f;
    }

    if ((key == GLFW_KEY_DOWN && action == GLFW_PRESS) || (key == GLFW_KEY_DOWN && action == GLFW_REPEAT))
    {
    	userData->offset -= 0.005f;
    }   

    if ((key == GLFW_KEY_LEFT && action == GLFW_PRESS) || (key == GLFW_KEY_LEFT && action == GLFW_REPEAT))
    {
    	userData->offset -= 0.005f;
    } 

    if ((key == GLFW_KEY_RIGHT && action == GLFW_PRESS) || (key == GLFW_KEY_RIGHT && action == GLFW_REPEAT))
    {
    	userData->offset += 0.005f;
    }     
    clampOffset(&(userData->offset));  
}

//clamp offset or hue to range [0,1] as required for the shader
void clampOffset(double* offset)
{
	if(*offset >= 1.0f)
	{
		*offset -= 1.0f;
	}
	else if(*offset <= 0.0f)
	{
		*offset += 1.0f;
	}
}