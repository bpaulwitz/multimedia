#!/bin/bash
echo -e "\033[32m"
echo "================================="
echo "====== Preparing GLFW/GLAD ======"
echo "================================="
echo -e "\033[39m"
curl https://cmake.org/files/v3.11/cmake-3.11.2-Darwin-x86_64.tar.gz --output cmake.tar.gz
gunzip -c cmake.tar.gz | tar xopf -
rm cmake.tar.gz
git clone https://github.com/glfw/glfw.git
./cmake-3.11.2-Darwin-x86_64/CMake.app/Contents/bin/cmake glfw/CMakeLists.txt
cd glfw
make
cd ..
git clone https://github.com/Dav1dde/glad.git
rm -rf cmake-3.11.2-Darwin-x86_64
cd glad
python -m glad --generator c --out-path=../
cd ..
rm -rf glad
cp glfw/src/libglfw3.a src/
cp -r glfw/include/GLFW include/
rm -rf glfw
echo -e "\033[32m"
echo "======================================"
echo "====== Finished GLFW/GLAD Setup ======"
echo "======================================"
echo -e "\033[39m"
echo "Include following headers in your source file:"
echo "======================="
echo "#include <glad/glad.h>"
echo "#include <GLFW/glfw3.h>"
echo "======================="
echo "Build with:"
echo -e "\033[33m"
echo "gcc main.c src/glad.c {additional c-files} -Iinclude -Lsrc -lglfw3 -framework AppKit -framework IOKit -framework CoreVideo"
echo " - or -  "
echo "make"
echo -e "\033[39m"
