#ifndef GLCALLS_H
#define GLCALLS_H

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <math.h>

#define ATTRIB_POSITION 0
#define ATTRIB_COLOR 1

typedef struct{
	GLuint programObject;
	GLuint vertexBufferObject;
	GLuint vertexArrayObject;
	double offset;
	float time;
} UserData;

typedef struct
{
	GLfloat position[3];
	//GLubyte color[3];
	GLfloat color[3];
} VertexData;

GLboolean init(GLFWwindow* window, int sizeData);
void draw(GLFWwindow* window, int sizeData);
void teardown(GLFWwindow* window);
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods);
void error_callback(int error, const char* description);

VertexData* generatePoints(int n, double offset, float time);

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods);
void clampOffset(double* offset);

#endif