#version 330 core
layout(location = 0) in vec4 vPosition;
layout(location = 1) in vec3 vColor;

out vec4 fColor;

//https://stackoverflow.com/questions/15095909/from-rgb-to-hsv-in-opengl-glsl
// All components are in the range [0…1], including hue.
// hsv.v is always 1.0 -> unecessary multiplication
// hsv.s is only 0.0 at first vertex and 1.0 elsewhere -> no interpolation required -> multiply to avoid branching
vec3 hsv2rgb(vec3 c)
{
    vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
    return (K.xxx - c.yyy) * K.xxx + clamp(p - K.xxx, 0.0, 1.0) * c.yyy;
}

void main()
{
	fColor = vec4(hsv2rgb(vColor), 0.0);
	gl_Position = vPosition;
}