#version 330 core

precision mediump float;

out vec4 outColor;

in lowp vec4 fColor;

void main()
{
	outColor = fColor;
}