#include <stdio.h>
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <string.h>
#include <stdio.h>
#include "glCalls.h"

int main(int argc, char const *argv[])
{
	//minimum of 3 points
	int length = 3;
	if(argc == 2)
	{
		length = atoi(argv[1]);
	}
	// this is our window, it also holds the openGL context
	GLFWwindow* window;

	// register error callback
	glfwSetErrorCallback(error_callback);

	// try init glfw
	if(!glfwInit())
	{
		printf("Failed to initialize GLFW\n");
		return -1;
	}

	// we want at least OpenGl 3.3
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);

	// enable forward-compability and core profile
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

	// define depth/stencil buffer
	glfwWindowHint(GLFW_DEPTH_BITS, 0);
	glfwWindowHint(GLFW_STENCIL_BITS, 0);

	// create window
	window = glfwCreateWindow(640, 480, "", NULL, NULL);

	if(!window)
	{
		glfwTerminate();
		return -1;
	}

	// makes the context of our window current aka this thread is now OpenGL rendering thread
	glfwMakeContextCurrent(window);

	// load pointers to OpenGL functions/extensions at runtime
	gladLoadGLLoader((GLADloadproc) glfwGetProcAddress);

	// draw a new image every frame
	glfwSwapInterval(1);

	// init OpenGL stuff
	if(!init(window, length))
	{
		glfwTerminate();
		return -1;
	}

	//register key callback (glfw)
	glfwSetKeyCallback(window, key_callback);

	printf("Use the arrow keys to rotate the colors on the polygon\n");

	// our render loop
	while(!glfwWindowShouldClose(window))
	{
		draw(window, length);
		glfwPollEvents();
	}

	// clean up
	teardown(window);

	//clean up glfw
	glfwDestroyWindow(window);
	glfwTerminate();

	return 0;
}