#ifndef GLCALLS_H
#define GLCALLS_H

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#define ATTRIB_POSITION 0
#define ATTRIB_COLOR 1

typedef struct{
	GLuint programObject;
	GLuint vertexBufferObject;
	GLuint vertexArrayObject;
} UserData;

typedef struct
{
	GLfloat position[3];
	GLubyte color[3];
} VertexData;

GLboolean init(GLFWwindow* window);
void draw(GLFWwindow* window);
void teardown(GLFWwindow* window);
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods);
void error_callback(int error, const char* description);
#endif