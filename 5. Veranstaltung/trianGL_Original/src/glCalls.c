#include "glCalls.h"
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

void glCheck(const char* errorText){
	GLenum error = glGetError();

	if ( error != GL_NO_ERROR ){
		printf("GLError: %s - %d\n", errorText, error);
		exit(EXIT_FAILURE);
	}
}

char* readShaderFromFile(char* filename) {
	FILE *fp;
	long lSize;
	char *buffer;

	fp = fopen ( filename , "rb" );
	assert(fp);

	fseek( fp , 0L , SEEK_END);
	lSize = ftell( fp );
	rewind( fp );

	/* allocate memory for entire content */
	buffer = calloc( 1, lSize + 1 );
	if ( !buffer ) fclose(fp), fputs("memory alloc fails", stderr), exit(1);

	/* copy the file into the buffer */
	if ( 1 != fread( buffer , lSize, 1 , fp) )
		fclose(fp), free(buffer), fputs("entire read fails", stderr), exit(1);

	return buffer;
}

GLuint compileShader(GLenum type, const char* shaderSource, char* shaderTag){

	// shader refernce
	GLuint shader;

	// create an empty shader object
	shader = glCreateShader(type);
	glCheck("glCreateShader");

	// put source code into empty shader object
	glShaderSource(shader, 1, &shaderSource, NULL);
	glCheck("glShaderSource");

	// compile the shader
	glCompileShader(shader);
	glCheck("glCompileShader");

	// compile status
	GLint compiled;

	// check compile status
	glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);
	glCheck("glGetShaderiv");

	if ( !compiled ){
		GLint infoLen = 0;
		
		// get length of error message
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLen);
		glCheck("glGetShaderiv GL_INFO_LOG_LENGTH");

		if ( infoLen > 1 ){
			char* errorMessage = malloc(sizeof(char*) * infoLen);

			// get error message
			glGetShaderInfoLog(shader, infoLen, NULL, errorMessage);

			printf("Error compiling shader (%s): %s\n", shaderTag, errorMessage);

			free(errorMessage);
		}	

		// delete shader object
		glDeleteShader(shader);
		glCheck("glDeleteShader");

		return 0;
	}

	return shader;
}

GLboolean init(GLFWwindow* window){
	
	// allocation memory for user data struct
	UserData* userData = malloc(sizeof(UserData));

	glfwSetWindowUserPointer(window, (void*)userData);

	// create vertex shader
	char* vertexShaderSource = readShaderFromFile("shader/vertex.glsl");
	GLuint vertexShader = compileShader(GL_VERTEX_SHADER, vertexShaderSource, "Vertex Shader");
	free(vertexShaderSource);

	// create fragment shader
	char* fragmentShaderSource = readShaderFromFile("shader/fragment.glsl");
	GLuint fragmentShader = compileShader(GL_FRAGMENT_SHADER, fragmentShaderSource, "Fragment Shader");
	free(fragmentShaderSource);

	// hint to release the shader compiler
	glReleaseShaderCompiler();
	glCheck("glReleaseShaderCompiler");

	if ( vertexShader == 0 || fragmentShader == 0 ){
		return GL_FALSE;
	}
	
	// program reference
	GLuint programObject;

	// create empty program object
	programObject = glCreateProgram();
	glCheck("glCreateProgram");

	// attach shaders ro profram object
	glAttachShader(programObject, vertexShader);
	glCheck("glAttachShader vertexShader");

	glAttachShader(programObject, fragmentShader);
	glCheck("glAttachShader fragmentShader");

	// link shaders
	glLinkProgram(programObject);
	glCheck("glLinkProgram");

	// detach shaders from program object
	glDetachShader(programObject, vertexShader);
	glCheck("glDetachShader");

	glDetachShader(programObject, fragmentShader);
	glCheck("glDetachShader");

	// delete shader objects
	glDeleteShader(vertexShader);
	glCheck("glDeleteShader");

	glDeleteShader(fragmentShader);
	glCheck("glDeleteShader");

	// link status
	GLint linked;

	// check linker status
	glGetProgramiv(programObject, GL_LINK_STATUS, &linked);
	glCheck("glGetProgramiv GL_LINK_STATUS");

	if ( !linked )
	{
		GLint infoLen;

		// get length of error message
		glGetProgramiv(programObject, GL_INFO_LOG_LENGTH, &infoLen);

		if ( infoLen > 1 ){
			char* errorMessage = malloc(sizeof(char) * infoLen);

			// get the error message
			glGetProgramInfoLog(programObject, infoLen, NULL, errorMessage);

			printf("Error linking program: %s\n", errorMessage);

			free(errorMessage);			
		}

		glDeleteProgram(programObject);
		glCheck("glDeleteProgram");

		return GL_FALSE;
	}

	// use program
	glUseProgram(programObject);
	glCheck("glUseProgram");

	// store program object 
	userData->programObject = programObject;

	// triange data
	VertexData vertexData[3];

	vertexData[0].position[0] = -1.0;
	vertexData[0].position[1] = -1.0;
	vertexData[0].position[2] =  0.0;
	vertexData[0].color[0]= 0xFF;
	vertexData[0].color[1]= 0x00;
	vertexData[0].color[2]= 0x00;

	vertexData[1].position[0] =  0.0;
	vertexData[1].position[1] =  1.0;
	vertexData[1].position[2] =  0.0;
	vertexData[1].color[0]= 0x00;
	vertexData[1].color[1]= 0xFF;
	vertexData[1].color[2]= 0x00;

	vertexData[2].position[0] =  1.0;
	vertexData[2].position[1] = -1.0;
	vertexData[2].position[2] =  0.0;
	vertexData[2].color[0]= 0x00;
	vertexData[2].color[1]= 0x00;
	vertexData[2].color[2]= 0xFF;

	// create and bind VAO
	glGenVertexArrays(1, &(userData->vertexArrayObject));
	glCheck("glGenVertexArrays");

	glBindVertexArray(userData->vertexArrayObject);
	glCheck("glBindVertexArray");

	// create VBO
	glGenBuffers(1, &(userData->vertexBufferObject));
	glCheck("glGenBuffers");

	glBindBuffer(GL_ARRAY_BUFFER, userData->vertexBufferObject);
	glCheck("glBindBuffer");

	// upload data to gpu
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertexData), (const GLvoid*) vertexData, GL_STATIC_DRAW);
	glCheck("glBufferData");

	// attribute index, component count, normalize, stride, pointer
	glVertexAttribPointer(ATTRIB_POSITION, 3, GL_FLOAT, GL_FALSE, sizeof(VertexData), (GLvoid*)offsetof(VertexData, position));
	glCheck("glVertexAttributePointer ATTRIB_POSITION");

	// enable vertex attribute
	glEnableVertexAttribArray(ATTRIB_POSITION);
	glCheck("glEnableVertexAttributeArray ATTRIB_POSITION");

	// attribute index, component count, normalize, stride, pointer
	glVertexAttribPointer(ATTRIB_COLOR, 3, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(VertexData), (GLvoid*)offsetof(VertexData, color));
	glCheck("glVertexAttributePointer ATTRIB_POSITION");

	// enable vertex attribute
	glEnableVertexAttribArray(ATTRIB_COLOR);
	glCheck("glEnableVertexAttributeArray ATTRIB_POSITION");

	// get the window size
	int width, height;
	glfwGetFramebufferSize(window, &width, &height);
	glCheck("glfwGetFrameBufferSize");

	// set viewport
	glViewport(0, 0, width, height);
	glCheck("glViewport");

	// define clear color
	glClearColor(0.2f, 0.2f, 0.2f, 0.0f);
	glCheck("glClearColor");


	return GL_TRUE;
}

void draw (GLFWwindow* window){
	// clear framebuffer
	glClear(GL_COLOR_BUFFER_BIT);
	glCheck("glClear");

	// TODO: draw triangle
	glDrawArrays(GL_TRIANGLES, 0, 3);
	glCheck("glDrawArrays");

	//swap framebuffer
	glfwSwapBuffers(window);

}

void teardown(GLFWwindow* window){

	UserData* userData = (UserData*)glfwGetWindowUserPointer(window);

	// delete VBO
	glDeleteBuffers(1, &(userData->vertexBufferObject));
	glCheck("glDeleteBuffers");

	// delete VBA
	glDeleteVertexArrays(1, &(userData->vertexArrayObject));
	glCheck("glDeleteVertexArrays");

	glDeleteProgram(userData->programObject);
	glCheck("glDeleteProgram");

	free(userData);

}

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods){

}

void error_callback(int error, const char* description){
    fprintf(stderr, "Error: %s\n", description);
}