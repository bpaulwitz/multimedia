#include <string.h>
#include <stdio.h>
#include <glad/glad.h>
#include "glCalls.h"

int main(void){

	// this is our window, it als holds the OpenGL context
	GLFWwindow* window;

	// register error callback
	glfwSetErrorCallback(error_callback);

	// try init glfw
	if(!glfwInit()){
		printf("Failed ro inint GLFW :(\n");
		return -1;
	}

	// we want at least OpenGL 4.1
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);

	// enable forward-compatibility and core profile
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

	// define depth/stencil buffer
	glfwWindowHint(GLFW_DEPTH_BITS, 0);
	glfwWindowHint(GLFW_STENCIL_BITS, 0);

	// create window
	window = glfwCreateWindow(640, 480, "Hello Triangle", NULL, NULL);
	if(!window){
		glfwTerminate();
		return -1;
	}

	// makes the context of our window current aka this thread is now OpenGL rendering thread
	glfwMakeContextCurrent(window);

	// load pointers to OpenGL functions/extensions at runtime
	gladLoadGLLoader((GLADloadproc) glfwGetProcAddress);

	// draw a new image every frame
	glfwSwapInterval(1);

	// init OpenGL stuff
	if(!init(window)){
		glfwTerminate();
		return -1;
	}

	// our render loop
	while(!glfwWindowShouldClose(window)){
		draw(window);
		glfwPollEvents();
	}

	// clean up
	teardown(window);

	// clean up glfw
	glfwDestroyWindow(window);
	glfwTerminate();


	return 0;
}