#ifndef GLCALLS_H
#define GLCALLS_H

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <math.h>

#define ATTRIB_POSITION 0
#define ATTRIB_COLOR 1

#define Y_ROT_SPEED 10.0
#define X_ROT_SPEED 10.0

typedef struct{
	GLuint programObject;
	GLuint vertexBufferObject;
	GLuint vertexArrayObject;
	GLuint vertexDataCount;
	double time;
	GLfloat angleY;
	GLint angleYLoc;
	GLfloat angleX;
	GLint angleXLoc;
} UserData;

typedef struct
{
	GLfloat position[3];
	GLubyte color[3];
} VertexData;

GLboolean init(GLFWwindow* window);
void update(GLFWwindow* window);
void draw(GLFWwindow* window);
void teardown(GLFWwindow* window);
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods);
void error_callback(int error, const char* description);
#endif
