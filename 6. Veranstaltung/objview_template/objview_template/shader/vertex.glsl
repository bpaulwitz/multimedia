#version 330
layout(location = 0) in vec4 vPosition;
layout(location = 1) in vec4 vColor;

out vec4 fColor;

#define M_PI 3.141592653

//uniforms
uniform float angle_y;
uniform float angle_x;
//uniform mat4 MPVmatrix;

void main()
{
/*

	//Some "good" perspective parameters:
	
	float near = 1.0;
	float far = 12.0;
	float left = -2.0;
	float right = 2.0;
	float top = 2.0;
	float bottom = -2.0; */

	//Also translate by (x: 0.0, y: -3.0, z: -6.0)

	float near = 1.0;
    float far = 12.0;
    float left = -2.0;
    float right = 2.0;
    float top = 2.0;
    float bottom = -2.0;


    mat4 frustum = mat4(
        2.0 * near / (right - left), 0.0, 0.0, 0.0,
        0.0, 2.0 * near / (top - bottom), 0.0, 0.0,
        (right + left) / (right - left), (top + bottom) / (top - bottom), -(far + near) / (far - near), -1.0,
        0.0, 0.0, -2.0 * far * near / (far - near), 0.0
    );

    mat4 rot_y = mat4(
    	cos(angle_y), 	0.0, 	-sin(angle_y), 	0.0,
    	0.0, 			1.0, 	0.0, 			0.0,
    	sin(angle_y), 	0.0, 	cos(angle_y), 	0.0,
    	0.0, 			0.0, 	0.0, 			1.0
    );

    mat4 rot_x = mat4(
    	1.0, 			0.0, 		0.0,		0.0,
    	0.0, 			cos(angle_x), 	-sin(angle_x), 			0.0,
    	0.0, 			sin(angle_x), 	cos(angle_x), 	0.0,
    	0.0, 			0.0, 	0.0, 			1.0
    );

    vec4 trans = vec4(0, -3, -6, 1);
	
	gl_Position = frustum * (rot_x *( rot_y * vPosition) + trans);
	fColor = vColor;
}
