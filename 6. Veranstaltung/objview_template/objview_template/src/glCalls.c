#include "glCalls.h"
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include "objImport.h"

void checkError(const char* errorText){
	GLenum error = glGetError();

	if(error != GL_NO_ERROR){
		printf("GLError: %s - %d\n", errorText, error);
		exit(EXIT_FAILURE);
	}
}

char* readShaderFromFile(char* filename) {
	FILE *fp;
	long lSize;
	char *buffer;

	fp = fopen ( filename , "rb" );
	assert(fp);

	fseek( fp , 0L , SEEK_END);
	lSize = ftell( fp );
	rewind( fp );

	/* allocate memory for entire content */
	buffer = calloc( 1, lSize + 1 );
	if ( !buffer ) fclose(fp), fputs("memory alloc fails", stderr), exit(1);

	/* copy the file into the buffer */
	if ( 1 != fread( buffer , lSize, 1 , fp) )
		fclose(fp), free(buffer), fputs("entire read fails", stderr), exit(1);

	return buffer;
}

GLuint compileShader(GLenum type, const char* shaderSource, char* shaderTag){

	//shader handle
	GLuint shader;

	//create empty shader object
	shader = glCreateShader(type);

	if(shader == 0) return 0;

	//put source code into empty shader object
	glShaderSource(shader, 1, &shaderSource, NULL);

	//compile shader
	glCompileShader(shader);

	//compile status
	GLint compiled;

	//check compile status
	glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);

	if( !compiled ){

		GLint infoLen = 0;

		//get length of error message
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLen);

		if( infoLen > 1){
			char* errorMsg = malloc(sizeof(char) * infoLen);

			//get error message
			glGetShaderInfoLog(shader, infoLen, NULL, errorMsg);

			printf("Error compiling shader (%s): %s\n", shaderTag, errorMsg);

			free(errorMsg);
		}

		// delete shader object
		glDeleteShader(shader);

		return GL_FALSE;
	}

	return shader;
}

GLboolean init(GLFWwindow* window){

	//allocate memory for user data
	UserData* userData = malloc(sizeof(UserData));
	glfwSetWindowUserPointer(window, (void*)userData);
	
	//Create the vertex shader:
	char* vertexShaderSource = readShaderFromFile("shader/vertex.glsl");

	GLuint vertexShader = compileShader(GL_VERTEX_SHADER, vertexShaderSource, "Vertex shader");

	free(vertexShaderSource);

	//Create the fragment shader:
	char* fragmentShaderSource = readShaderFromFile("shader/fragment.glsl");

	GLuint fragmentShader = compileShader(GL_FRAGMENT_SHADER, fragmentShaderSource, "Fragment shader");

	free(fragmentShaderSource);

	// hint to release resources of shader compiler
	glReleaseShaderCompiler();

	//program handle
	GLuint programObject;

	//create empty program object
	programObject = glCreateProgram();

	if(programObject == 0) return 0;

	// attach shaders to program object
	glAttachShader(programObject, vertexShader);
	glAttachShader(programObject, fragmentShader);

	// link shaders
	glLinkProgram(programObject);
	checkError("glLinkProgram");

	// detach shaders from program object
	glDetachShader(programObject, vertexShader);
	glDetachShader(programObject, fragmentShader);

	// delete shader objects
	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);

	// link status
	GLint linked;

	// check link status
	glGetProgramiv(programObject, GL_LINK_STATUS, &linked);
	checkError("glGetProgramiv");

	if( !linked ){

		GLint infoLen;

		//get length of linker error message
		glGetProgramiv(programObject, GL_INFO_LOG_LENGTH, &infoLen);

		if( infoLen > 1){
			char* errorMsg = malloc(sizeof(char) * infoLen);

			//get error message from program object
			glGetProgramInfoLog(programObject, infoLen, NULL, errorMsg);

			printf("Error linking program: %s\n", errorMsg);

			free(errorMsg);
		}

		glDeleteProgram(programObject);

		return GL_FALSE;
	}

	// use program
	glUseProgram(programObject);
	checkError("glUseProgram");

	// store program object in context
	userData->programObject = programObject;

	//get uniform locations
	userData->angleXLoc = glGetUniformLocation(userData->programObject, "angle_x");
	checkError("glGetUniformLocation (angle_x)");
	assert(userData->angleYLoc >= 0);

	userData->angleYLoc = glGetUniformLocation(userData->programObject, "angle_y");
	checkError("glGetUniformLocation (angle_y)");
	assert(userData->angleYLoc >= 0);

	//initialize the modell
	userData->time = glfwGetTime();
	userData->angleY = 0.0f;
	userData->angleX = 0.0f;

	// create and bind dummy vao... it is needed in desktop OpenGL
	glGenVertexArrays(1, &(userData->vertexArrayObject));
	checkError("glGenVertexArray");

	glBindVertexArray(userData->vertexArrayObject);
	checkError("glBindVertexArray");

	// generate a VBO
	glGenBuffers(1, &(userData->vertexBufferObject));
	checkError("glGenBuffers");

	glBindBuffer(GL_ARRAY_BUFFER, userData->vertexBufferObject);
	checkError("glBindBuffer");

	//Open the obj file:
	const char* objFilePath = "teapot";
	//TODO
	const FILE* objFile = fopen(objFilePath, "r");
	assert(objFile);

	//Count the entries:
	int vertexCount = 0;
	int texCoordsCount = 0;
	int normalCount = 0;
	int faceCount = 0;
	int mtlLibCount = 0;

	//TODO
	obj_count_entries(objFile, &vertexCount, &texCoordsCount, &normalCount, &faceCount, &mtlLibCount);

	//Print some info:
	printf("Parsed obj file \"%s\":\n", objFilePath);
	printf("\tVertices: %d\n", vertexCount);
	printf("\tTexture coordinates: %d\n", texCoordsCount);
	printf("\tNormals: %d\n", normalCount);
	printf("\tFaces: %d\n", faceCount);
	printf("\tMaterial libraries: %d\n", mtlLibCount);

	//Rewind the file pointer:
	//TODO
	rewind(objFile);

	//Allocate memory for array of type obj_vertex_entry_t:
	//TODO
	obj_vertex_entry_t* vertices = (obj_vertex_entry_t*)malloc(vertexCount*sizeof(obj_vertex_entry_t)); 
	assert(vertices);

	//Store the number of vertices into user data & allocate vertex data array:
	//TODO
	int VertexDataCount = 3 * faceCount;
	userData->vertexDataCount = VertexDataCount;
	VertexData* vertexDataArray = malloc(userData->vertexDataCount * sizeof(VertexData));

	//Iterate over the entries until OBJ_ENTRY_TYPE_END and build vertex data (OBJ_ENTRY_TYPE_VERTEX and OBJ_ENTRY_TYPE_FACE):
	//TODO
	obj_entry_t currentEntry;
	obj_entry_type_t currentType;
	int loopVertexCounter = 0;
	int loopFaceCounter = 0;
	//VertexData nextData;
	while((currentType = obj_get_next_entry(objFile, &currentEntry)) != OBJ_ENTRY_TYPE_END)
	{
		switch(currentType)
		{
			case OBJ_ENTRY_TYPE_VERTEX:
				vertices[loopVertexCounter++] = currentEntry.vertex_entry;
				break;

			case OBJ_ENTRY_TYPE_FACE:
				{
					obj_face_entry_t faceEntry = currentEntry.face_entry;

					for (int i = 0; i < 3; ++i)
					{
						//Get the vertex for the current index [1, 3]
						int vertexIndex = faceEntry.triples[i].vertex_index;
						obj_vertex_entry_t vertexEntry = vertices[vertexIndex];

						//Build Vertex Data for it
						VertexData new_vertex_data = 
						{
							.position = 
							{
								vertexEntry.x,
								vertexEntry.y,
								vertexEntry.z
							},
							.color = 
							{
								0x10,
								0x10,
								0x10
							}
						};

						//Store the new vertex data
						vertexDataArray[loopFaceCounter++] = new_vertex_data;
					}
				}

				//nextData.position[0] = vertices[currentEntry.face_entry.triples[0].vertex_index].x;
				//nextData.position[1] = vertices[currentEntry.face_entry.triples[0].vertex_index].y;
				//nextData.position[2] = vertices[currentEntry.face_entry.triples[0].vertex_index].z;
				//nextData.color[0] = 0x10;
				//nextData.color[1] = 0x10;
				//nextData.color[2] = 0x10;
				//vertexDataArray[loopFaceCounter++] = nextData;

				//nextData.position[0] = vertices[currentEntry.face_entry.triples[1].vertex_index].x;
				//nextData.position[1] = vertices[currentEntry.face_entry.triples[1].vertex_index].y;
				//nextData.position[2] = vertices[currentEntry.face_entry.triples[1].vertex_index].z;
				//nextData.color[0] = 0x10;
				//nextData.color[1] = 0x10;
				//nextData.color[2] = 0x10;
				//vertexDataArray[loopFaceCounter++] = nextData;

				//nextData.position[0] = vertices[currentEntry.face_entry.triples[2].vertex_index].x;
				//nextData.position[1] = vertices[currentEntry.face_entry.triples[2].vertex_index].y;
				//nextData.position[2] = vertices[currentEntry.face_entry.triples[2].vertex_index].z;
				//nextData.color[0] = 0x10;
				//nextData.color[1] = 0x10;
				//nextData.color[2] = 0x10;
				//vertexDataArray[loopFaceCounter++] = nextData;
				break;

			default:
				break;
		}
	}

	//Buffer the data to the GPU:
	//TODO
	glBufferData(GL_ARRAY_BUFFER, sizeof(VertexData) * VertexDataCount, (const GLvoid*) vertexDataArray, GL_STATIC_DRAW);

	//Release stuff and close the file:
	//TODO
	free(vertices);
	free(vertexDataArray);
	fclose(objFile);

	//attribute index, component count, normalize, stride, pointer
	glVertexAttribPointer(ATTRIB_POSITION, 3, GL_FLOAT, GL_FALSE, sizeof(VertexData), (GLvoid*)offsetof(VertexData, position));
	checkError("glVertexAttribPointer - ATTRIB_POSITION");

	//enable vertex attribute
	glEnableVertexAttribArray(ATTRIB_POSITION);
	checkError("glEnableVertexAttribArray - ATTRIB_POSITION");

	//attribute index, component count, normalize, stride, pointer
	glVertexAttribPointer(ATTRIB_COLOR, 3, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(VertexData), (GLvoid*)offsetof(VertexData, color));
	checkError("glVertexAttribPointer - ATTRIB_COLOR");

	//enable vertex attribute
	glEnableVertexAttribArray(ATTRIB_COLOR);
	checkError("glEnableVertexAttribArray - ATTRIB_COLOR");

	// get the window size
	int width, height;
	glfwGetFramebufferSize(window, &width, &height);

	// set viewport
	glViewport(0, 0, width, height);
	checkError("glViewport");

	// define clear for color buffer with black color
	glClearColor(0.2f,0.2f,0.2f,0.0f);
	checkError("glClearColor");

	return GL_TRUE;
}

void update(GLFWwindow* window)
{
	UserData* userData = (UserData*) glfwGetWindowUserPointer(window);

	//calculate time delta
	double newTime = glfwGetTime();
	double timeDelta = newTime - userData->time;
	userData->time = newTime;

	userData->angleX = fmod(userData->angleX + (X_ROT_SPEED * timeDelta), 2 * M_PI);

	//update angle y:
	//Fmod = float modulo, [0.0, 2pi]
	userData->angleY = fmod(userData->angleY + (Y_ROT_SPEED * timeDelta), 2 * M_PI);

	//update uniforms
	glUniform1f(userData->angleYLoc, userData->angleY);
	checkError("glUniform1f (angleY)");
}

void draw (GLFWwindow* window){
	UserData* userData = (UserData*) glfwGetWindowUserPointer(window);

	// clear color buffer
	glClear(GL_COLOR_BUFFER_BIT);

	// draw stuff
	// primitive type, start index in array, number of elements to render
	glDrawArrays(GL_TRIANGLES, 0, userData->vertexDataCount);
	
	//TODO

    glfwSwapBuffers(window);
}

void teardown(GLFWwindow* window){
	UserData* userData = (UserData*) glfwGetWindowUserPointer(window);

	glDeleteBuffers(1, &(userData->vertexBufferObject));
	checkError("glDeleteBuffers");

	glDeleteVertexArrays(1, &(userData->vertexArrayObject));
	checkError("glDeleteVertexArrays");

	free(userData);
}

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods){

}

void error_callback(int error, const char* description){
    fprintf(stderr, "Error: %s\n", description);
}