#include "honkpacker.h"

void writeByte(FILE* output, int byte)
{
	if(fputc(byte, output) == EOF)
	{
		printf("Writing error\n");
		exit(EXIT_FAILURE);
	}
}

void writeOutHom(FILE* output, int length, int byte)
{
	writeByte(output, length | 0x80);
	writeByte(output, byte);
}

void writeOutHet(FILE* output, int length, int* buffer)
{
	writeByte(output, length);
	for (int i = 0; i < length; ++i)
	{
		writeByte(output, buffer[i]);
	}
}

void honkCompress(FILE* in, FILE* out)
{
	//Initial State
	honk_state_t state = HONK_STATE_INIT;

	//length of current block, current byte, last byte
	int length, result, lastResult;
	//buffer for homogen block
	int buffer[127];

	while(1)
	{
		//read current byte
		result = fgetc(in);
		//when end of file is reached, write last bytes according to the state
		if(result == EOF)
		{
			switch(state)
			{
				case HONK_STATE_INIT:
				break;

				case HONK_STATE_TMP:
					buffer[0] = lastResult;
					writeOutHet(out, 1, buffer);
				break;

				case HONK_STATE_HOM:
					writeOutHom(out, length - 1, lastResult);
				break;

				case HONK_STATE_HET:
					writeOutHet(out, length - 1, buffer);
				break;
			}
			break;
		}
		else
		{
			switch(state)
			{
				//Initial state: set last result
				case HONK_STATE_INIT:
					lastResult = result;
					state = HONK_STATE_TMP;
				break;

				//temporary state: compare with last result
				case HONK_STATE_TMP:
					length = 2;
					//if bytes equal -> homogen block
					if(lastResult == result)
					{
						state = HONK_STATE_HOM;
					}
					//else heterogen block
					else
					{
						buffer[0] = lastResult;
						buffer[1] = result;
						lastResult = result;
						state = HONK_STATE_HET;
					}
				break;

				//homogen state: block of equal bytes
				case HONK_STATE_HOM:
					//equal -> stay in state
					if(lastResult == result)
					{
						length++;
						//if block full -> go to initial state to start new block
						if(length >= 127)
						{
							writeOutHom(out, 0xFF, result);
							state = HONK_STATE_INIT;
						}
					}
					//unequal -> write block and go to temporary state to compare with the next byte
					else
					{
						writeOutHom(out, length, lastResult);
						lastResult = result;
						state = HONK_STATE_TMP;
					}
				break;

				//heterogen state: block of unequal bytes
				case HONK_STATE_HET:
					//unequal -> stay in state
					if(lastResult != result)
					{
						buffer[length] = result;
						length++;
						lastResult = result;
						//block full -> go to initial state
						if(length >= 127)
						{
							writeOutHet(out, 127, buffer);
							state = HONK_STATE_INIT;
						}
					}
					//equal -> switch to homogen state
					else
					{
						writeOutHet(out, length - 1, buffer);
						length = 2;
						state = HONK_STATE_HOM;
					}
				break;
			}
		}
	}
}

void honkDecompress(FILE* in, FILE* out)
{
	honk_state_t state = HONK_STATE_INIT;
	int length, result;
	while(1)
	{
		result = fgetc(in);
		if(result == EOF)
		{
			break;
		}

		switch(state)
		{
			case HONK_STATE_INIT:
				length = result & 0x7F;
				if(result >> 7)
					state = HONK_STATE_HOM;
				else
					state = HONK_STATE_HET;
				break;

			case HONK_STATE_HOM:
				for (int i = 0; i < length; ++i)
				{
					writeByte(out, result);
				}
				state = HONK_STATE_INIT;
				break;

			case HONK_STATE_HET:
				writeByte(out, result);
				length--;
				if(length == 0)
					state = HONK_STATE_INIT;
				break;
		}
	}
	
	//lesefehler
	if(ferror(in))
	{
		printf("Error while reading the file\n");
		exit(EXIT_FAILURE);
	}
}