#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "bitmap.h"
#include "honkpacker.h"

//Each component must be >= 8.
int default_quant_matrix[64] =
{
	16,  11,  10,  16,  24,  40,  51,  61,
	12,  12,  14,  19,  26,  58,  60,  55,
	14,  13,  16,  24,  40,  57,  69,  56,
	14,  17,  22,  29,  51,  87,  80,  62,
	18,  22,  37,  56,  68, 109, 103,  77,
	24,  35,  55,  64,  81, 104, 113,  92,
	49,  64,  78,  87, 103, 121, 120, 101,
	72,  92,  95,  98, 112, 100, 103,  99
};

int zig_zag_index_matrix[64] =
{
	 0,  1,  5,  6, 14, 15, 27, 28, //0
	 2,  4,  7, 13, 16, 26, 29, 42, //8
	 3,  8, 12, 17, 25, 30, 41, 43, //16
	 9, 11, 18, 24, 31, 40, 44, 53, //24
	10, 19, 23, 32, 39, 45, 52, 54, //32
	20, 22, 33, 38, 46, 51, 55, 60, //40
	21, 34, 37, 47, 50, 56, 59, 61, //48
	35, 36, 48, 49, 57, 58, 62, 63 //56
};

int zag_zig_index_matrix[64] =
{
	 0,  1,  8, 16,  9,  2,  3, 10,
	17, 24, 32, 25, 18, 11,  4,  5,
	12, 19, 26, 33, 40, 48, 41, 34,
	27, 20, 13,  6,  7, 14, 21, 28,
	35, 42, 49, 56, 57, 50, 43, 36,
	29, 22, 15, 23, 30, 37, 44, 51, 
	58, 59,	52, 45, 38, 31, 39, 46, 
	53, 60,	61, 54, 47, 55, 62, 63
};

//linear interpolation
inline int lerp(float min, float max, float t) 
{
    return (min * (1.0 - t)) + (max * t);
}

//adapt matrix with a percentage in range [0, 100]
static void adaptMatrix(int* matrix, int percentage)
{
	int range;
	float mat_percent, result_percent;
	int highest = 0, smallest = 16;

	//get highest and lowest value from matrix
	for (int i = 0; i < 64; ++i)
	{
		if(matrix[i] > highest)
		{
			highest = matrix[i];
		}
		if(matrix[i] < smallest)
		{
			smallest = matrix[i];
		}
	}

	for (int i = 0; i < 64; ++i)
	{
		//percentage of value in matrix
		mat_percent = matrix[i] / (highest - smallest);
		if((mat_percent + ((float)percentage)/100) < 100)
		{
			result_percent = mat_percent + ((float)percentage)/100;
		}
		else
		{
			result_percent = 100;
		}

		matrix[i] = lerp(smallest, highest, result_percent);
	}
}


//https://stackoverflow.com/questions/4833347/removing-substring-from-a-string
static void remove_substring(char *s,const char *toremove)
{
  while( (s=strstr(s,toremove)) )
    memmove(s,s+strlen(toremove),1+strlen(s+strlen(toremove)));
}

static char* concat(const char *s1, const char *s2)
{
    char *result = malloc(strlen(s1) + strlen(s2) + 1); // +1 for the null-terminator
    // in real code you would check for errors in malloc here
    strcpy(result, s1);
    strcat(result, s2);
    return result;
}

static void read_block(const bitmap_pixel_hsv_t* pixels, int x, int y, int blocks_x, int blocks_y, float* block)
{
	int bytes_per_row = blocks_x * 8;
	int base_offset = y * 8 * bytes_per_row;
	int col_offset = x * 8;

	for (int curr_y = 0; curr_y < 8; curr_y++)
	{
		int row_offset = curr_y * bytes_per_row;

		for (int curr_x = 0; curr_x < 8; curr_x++)
		{
			block[(8 * curr_y) + curr_x] = pixels[base_offset + row_offset + col_offset + curr_x].v - 128.0f;
		}
	}
}

//like above
static void read_decompressed_block(const int8_t* compressed_pixels, int x, int y, int blocks_x, int blocks_y, int8_t* block)
{
	int bytes_per_row = blocks_x * 8;
	int base_offset = y * 8 * bytes_per_row;
	int col_offset = x * 8;

	for (int curr_y = 0; curr_y < 8; curr_y++)
	{
		int row_offset = curr_y * bytes_per_row;

		for (int curr_x = 0; curr_x < 8; curr_x++)
		{
			block[(8 * curr_y) + curr_x] = (float)(compressed_pixels[base_offset + row_offset + col_offset + curr_x]);
		}
	}
}

//save an input block to an array of bitmap_pixel_hsv_t pixels
static void save_to_output(const float* input_block, int x, int y, int blocks_x, int blocks_y, bitmap_pixel_hsv_t* pixels)
{
	int bytes_per_row = blocks_x * 8;
	int base_offset = y * 8 * bytes_per_row;
	int col_offset = x * 8;

	for (int curr_y = 0; curr_y < 8; curr_y++)
	{
		int row_offset = curr_y * bytes_per_row;

		for (int curr_x = 0; curr_x < 8; curr_x++)
		{
			pixels[base_offset + row_offset + col_offset + curr_x].h = 0;
			pixels[base_offset + row_offset + col_offset + curr_x].s = 0;
			//add 128 to the value to correct the pixel range from [-127, 128] to [0, 255]
			pixels[base_offset + row_offset + col_offset + curr_x].v = input_block[(8 * curr_y) + curr_x] + 128.0f;
		}
	}
}

//create grayscale bitmap of given image and save it
static bitmap_pixel_hsv_t* create_grayscale_bitmap(const char* input_path, 
	const char* grayscale_output_path, int* blocks_x, int* blocks_y)
{
	bitmap_pixel_hsv_t* pixels;
	int width_px, height_px;

	switch (bitmapReadPixels(input_path, (bitmap_pixel_t**)&pixels, 
		&width_px, &height_px, BITMAP_COLOR_SPACE_HSV))
	{
	case BITMAP_ERROR_INVALID_PATH:

		printf("Invalid path!\n");
		return NULL;

	case BITMAP_ERROR_INVALID_FILE_FORMAT:

		printf("Invalid file format!\n");
		return NULL;

	case BITMAP_ERROR_IO:

		printf("I/O error!\n");
		return NULL;

	case BITMAP_ERROR_MEMORY:

		printf("OOM\n");
		return NULL;
	}

	//Ensure that the bitmap is of dimension 8m X 8n:
	if ((width_px % 8) || (height_px % 8))
	{
		printf("Width and height must be a multiple of 8 pixels!\n");
		free(pixels);

		return NULL;
	}

	//Calculate the block dims:
	*blocks_x = width_px / 8;
	*blocks_y = height_px / 8;

	//Convert to grayscale (=> only keep the V of HSV):
	for (int i = 0; i < (width_px * height_px); i++)
	{
		pixels[i].h = 0;
		pixels[i].s = 0;
	}

	//Dump the bitmap:
	bitmap_parameters_t parameters =
	{
		.bottomUp = BITMAP_BOOL_TRUE,
		.widthPx = width_px,
		.heightPx = height_px,
		.colorDepth = BITMAP_COLOR_DEPTH_32,
		.compression = BITMAP_COMPRESSION_NONE,
		.dibHeaderFormat = BITMAP_DIB_HEADER_INFO,
		.colorSpace = BITMAP_COLOR_SPACE_HSV
	};

	//TODO: Improve error handling!
	if (bitmapWritePixels(grayscale_output_path, BITMAP_BOOL_TRUE, 
		&parameters, (bitmap_pixel_t*)pixels) != BITMAP_ERROR_SUCCESS)
	{
		printf("Failed to write grayscale output!\n");
		free(pixels);

		return NULL;
	}

	return pixels;
}

//see slides
static float dct_coeff(int idx)
{
	return (idx == 0) ? (sqrtf(2.0f) / 4.0f) : 0.5f;
}

//see slides
static float dct_sum(const float* input_block, int p, int q)
{
	float sum = 0;

	for (int n = 0; n < 8; n++)
	{
		for (int m = 0; m < 8; m++)
		{
			float input = input_block[(8 * n) + m];
			float cos0 = cosf(M_PI * ((2 * m) + 1) * p / 16.0f);
			float cos1 = cosf(M_PI * ((2 * n) + 1) * q / 16.0f);

			sum += input * cos0 * cos1;
		}
	}
	return sum;
}

//see slides
static void perform_dct(const float* input_block, float* output_block)
{
	for (int q = 0; q < 8; q++)
	{
		for (int p = 0; p < 8; p++)
		{
			float ap = dct_coeff(p);
			float aq = dct_coeff(q);
			float sum = dct_sum(input_block, p, q);

			output_block[(8 * q) + p] = ap * aq * sum;
		}
	}
}

//see slides
static float idct_sum(const float* input_block, int n, int m)
{
	float sum = 0;
	for (int q = 0; q < 8; q++)
	{
		for (int p = 0; p < 8; p++)
		{
			float input = input_block[(8 * q) + p];

			float ap = dct_coeff(p);
			float aq = dct_coeff(q);
		
			float cos0 = cosf(M_PI * ((2 * m) + 1) * p / 16.0f);
			float cos1 = cosf(M_PI * ((2 * n) + 1) * q / 16.0f);

			sum += cos0 * cos1 * ap * aq * input;
		}
	}

	return sum;
}

//see slides
static void perform_idct(const float* input_block, float* output_block)
{
	for(int n = 0; n < 8; n++)
	{
		for(int m = 0; m < 8; m++)
		{
			output_block[(8 * n) + m] = idct_sum(input_block, n, m);
		}
	}
}

static void quantize(const float* input_block, 
	const int* quant_matrix, int8_t* output_block)
{
	for (int i = 0; i < 64; i++)
	{
		//WARNING: Check quant_matrix for entries < 8    -> given with default matrix and adaptMatrix implemented like above
		output_block[i] = (int8_t)round(input_block[i] / quant_matrix[i]);
	}
}

//reversed quantize operation y=x/q -> x=y*q
static void dequantize(const int8_t* input_block,
	const int* quant_matrix, float* output_block)
{
	for (int i = 0; i < 64; ++i)
	{
		output_block[i] = ((float)(input_block[i])) * quant_matrix[i];
	}
}

//compressing
static void zig_zag(const int8_t* input_block, int8_t* output_block)
{
	for (int i = 0; i < 64; i++)
	{
		int idx = zig_zag_index_matrix[i];
		output_block[idx] = input_block[i];
	}
}

//decompressing
static void zag_zig(const int8_t* input_block, int8_t* output_block)
{
	for (int i = 0; i < 64; i++)
	{
		int idx = zag_zig_index_matrix[i];
		output_block[idx] = input_block[i];
	}
}

int compress(const char* input_path, const int* quant_matrix, const int percentage,
	const char* grayscale_output_path, const char* dct_output_path)
{
	//Load the bitmap file:
	int blocks_x, blocks_y;
	bitmap_pixel_hsv_t* pixels = create_grayscale_bitmap(input_path, 
		grayscale_output_path, &blocks_x, &blocks_y);

	if (!pixels)
	{
		return EXIT_FAILURE;
	}

	//Allocate space for the output file:
	size_t header_size = 2 * sizeof(uint32_t);
	size_t output_size = header_size + (blocks_x * blocks_y * 64);
	int8_t* output = malloc(output_size);

	if (!output)
	{
		printf("OOM!\n");
		free(pixels);

		return EXIT_FAILURE;
	}

	//adapt quantization matrix accordingly
	adaptMatrix(default_quant_matrix, percentage);

	//Write header:
	*(uint32_t*)(output + 0) = blocks_x;
	*(uint32_t*)(output + sizeof(uint32_t)) = blocks_y;

	//Iterate over all blocks:
	for (int y = 0; y < blocks_y; y++)
	{
		float input_block[64]; //-128 ... 127
		float dct_block[64]; //-1024 ... 1016
		int8_t quantized_block[64]; //-128 ... 127
		int8_t zig_zagged_block[64]; //-128 ... 127

		for (int x = 0; x < blocks_x; x++)
		{
			//Read the current input block:
			read_block(pixels, x, y, blocks_x, blocks_y, input_block);

			//Perform the DCT on it:
			perform_dct(input_block, dct_block);

			//Quantize:
			quantize(dct_block, quant_matrix, quantized_block);

			//ZigZag:
			zig_zag(quantized_block, zig_zagged_block);

			//Write the block to the output buffer:
			memcpy(output + header_size + (((y * blocks_x) + x) * 64), 
				zig_zagged_block, 64);
		}
	}

	//Free the pixels (not needed after this point):+
	free(pixels);

	//Save the output:
	FILE* output_file = fopen(dct_output_path, "wb");

	if (!output_file)
	{
		printf("Failed to create output file!\n");
		free(output);

		return EXIT_FAILURE;
	}

	if (fwrite(output, output_size, 1, output_file) != 1)
	{
		printf("Failed to write to output file!\n");

		fclose(output_file);
		free(output);

		return EXIT_FAILURE;
	}

	//Close file and free heap memory:
	fclose(output_file);

	output_file = fopen(dct_output_path, "rb");

	free(output);

	FILE* compressed_output_file = fopen(concat(dct_output_path, ".almostJPG"), "wb");
	honkCompress(output_file, compressed_output_file);
	fclose(compressed_output_file);
	fclose(output_file);

	return 0;
}

int decompress(const char* input_path, const int* quant_matrix, const int percentage,
	const char* dct_output_path, const char* output_path)
{
	FILE* in, *out, *tmp;
	//8 byte header with blocks_x and blocks_y
	char header[2 * sizeof(uint32_t)];
	uint32_t blocks_x, blocks_y;
	int8_t* compressed_pixels;
	bitmap_pixel_hsv_t* output_pixels;
	//counter for while loop 
	int counter = 0;
	int char_result;

	//Decompress honk compressed file
	in = fopen(input_path, "rb");
	//tmp = tmpnam(NULL);
	tmp = tmpfile();
	//tmp = fopen(dct_output_path, "wb");

	if (!in)
	{
		printf("Failed to read input file!\n");
		free(in);

		return EXIT_FAILURE;
	}
	else if(!tmp)
	{
		printf("Failed to create temporary .dct file!\n");
		free(tmp);

		return EXIT_FAILURE;
	}

	//decompressing and closing files
	honkDecompress(in, tmp);
	fclose(in);

	//read tmp file to decompress .dct file
	//reset filepointer
	rewind(tmp);

	//read header
	for (int i = 0; i < 2*(sizeof(uint32_t)); ++i)
	{
		char_result = fgetc(tmp);

		if(char_result == EOF)
		{
			printf("Error while reading block size\n");
			return EXIT_FAILURE;
		}

		header[i] = char_result;
	}
	//save block sizes from header in blocks_x and blocks_y
	blocks_x = *((uint32_t*)header);
	blocks_y = *((uint32_t*)(header + sizeof(uint32_t)));

	if(blocks_x == 0 && blocks_y == 0)
	{
		printf("No pixel body in file\n");
		return EXIT_FAILURE;
	}

	//adapt quantization matrix accordingly
	adaptMatrix(default_quant_matrix, percentage);

	//allocate space for compressed pixels array and output array
	compressed_pixels = (int8_t*)malloc(blocks_x * blocks_y * 64);
	output_pixels = (int8_t*)malloc(blocks_x * blocks_y * 64 * sizeof(bitmap_pixel_hsv_t));

	//fill array with compressed pixels from the temporary file
	while(1)
	{
		char_result = fgetc(tmp);

		if(char_result == EOF)
		{
			break;
		}

		compressed_pixels[counter] = char_result;
		counter++;
	}

	for (int y = 0; y < blocks_y; y++)
	{
		int8_t input_block[64]; //-128 ... 127
		int8_t zag_zigged_block[64]; //-128 ... 127
		float dequantized_block[64]; //-1024 ... 1016
		float idct_block[64]; //-128 ... 127
		

		for (int x = 0; x < blocks_x; x++)
		{
			//Read the current input block:
			read_decompressed_block(compressed_pixels, x, y, blocks_x, blocks_y, input_block);

			//ZagZig / reversed zigZag
			zag_zig(input_block, zag_zigged_block);

			//Dequantize:
			dequantize(zag_zigged_block, quant_matrix, dequantized_block);

			//Perform IDCT on it:
			perform_idct(dequantized_block, idct_block);

			//write to output pixels
			save_to_output(idct_block, x, y, blocks_x, blocks_y, output_pixels);

			//Write the block to the output buffer:
			//memcpy(output + (((y * blocks_x) + x) * 64), 
			//	idct_block, 64);
		}
	}

	free(compressed_pixels);

	//Dump the bitmap:
	bitmap_parameters_t parameters =
	{
		.bottomUp = BITMAP_BOOL_TRUE,
		.widthPx = blocks_x * 8,
		.heightPx = blocks_y * 8,
		.colorDepth = BITMAP_COLOR_DEPTH_32,
		.compression = BITMAP_COMPRESSION_NONE,
		.dibHeaderFormat = BITMAP_DIB_HEADER_INFO,
		.colorSpace = BITMAP_COLOR_SPACE_HSV
	};
	bitmapWritePixels(output_path, 
				BITMAP_BOOL_TRUE,
				&parameters,
				(bitmap_pixel_t*)output_pixels);

	return 0;
}

int main(int argc, char** argv)
{
	int compression_mode = -1;
	int percentage;
	char* input_path;
	char* grayscale_output_path;
	char* dct_output_path;
	char* output_path;

	//saving original input
	char* initial_input = (char*)malloc(500);
	//loop counter
	int current = 0;
	//current char in loop to copy
	char copy_char;
	while((copy_char = argv[4][current]) /*!= 0x0*/)
	{
		initial_input[current] = copy_char;
		current++;
	}
	current = 0;

	//remove ending of file name
	while((copy_char = argv[4][current]) != '.' || copy_char == 0x0)
	{
		current++;
	}
	argv[4][current] = 0x0;

	//Command line input
	if(argc != 5 || strcmp(argv[1], "-p") != 0)
	{
		printf("Necessary parameters:\n[-p][percentage][-d/-c][filepath]\n");
		return -1;
	}
	else if(atoi(argv[2]) < 0 || atoi(argv[2]) > 100)
	{
		printf("Necessary parameters:\n[-p][percentage][-d/-c][filepath]\npercentage must be between 0 and 100\n");
		return -1;
	}
	//either decompressing or compressing
	else if(strcmp(argv[3], "-c") == 0)
	{
		compression_mode = 1;
	}
	else if(strcmp(argv[3], "-d") == 0)
	{
		compression_mode = 0;
	}
	else
	{
		printf("Error in setting compression mode\n");
		return -1;
	}

	if(compression_mode < 0)
	{
		return -1;
	}
	//end reading command line input

	input_path = initial_input;

	//set filenames of output files
	grayscale_output_path = concat(argv[4], "_grayscale.bmp");
	dct_output_path = concat(argv[4], ".dct");
	output_path = concat(argv[4], "_decompressed.bmp");

	percentage = atoi(argv[2]);

	if (compression_mode)
	{
		return compress(input_path, default_quant_matrix, percentage,
			grayscale_output_path, dct_output_path);
	}
	else
	{
		return decompress(input_path, default_quant_matrix, percentage,
			dct_output_path, output_path);
	}
}