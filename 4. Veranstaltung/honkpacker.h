#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef enum __honk_state_t__
{
	HONK_STATE_INIT,
	HONK_STATE_TMP,
	HONK_STATE_HOM,
	HONK_STATE_HET
} honk_state_t;

void writeByte(FILE* output, int byte);

void writeOutHom(FILE* output, int length, int byte);

void writeOutHet(FILE* output, int length, int* buffer);

void honkCompress(FILE* in, FILE* out);

void honkDecompress(FILE* in, FILE* out);