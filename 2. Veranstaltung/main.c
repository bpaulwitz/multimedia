#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <xmmintrin.h>
#include <emmintrin.h>
#include <immintrin.h>
#include <time.h>
#include "bitmap.h"

//sse2 doesn't support 8bit-multiplication, so we used a solution from 
//https://stackoverflow.com/questions/8193601/sse-multiplication-16-x-uint8-t
__m128i sse2_8bit_mul(__m128i a, __m128i b)
{
    // unpack and multiply
    __m128i dst_even = _mm_mullo_epi16(a, b);
    __m128i dst_odd = _mm_mullo_epi16(_mm_srli_epi16(a, 8),_mm_srli_epi16(b, 8));

    return _mm_or_si128(_mm_slli_epi16(dst_odd, 8), _mm_srli_epi16(_mm_slli_epi16(dst_even,8), 8));
}

//https://stackoverflow.com/questions/4833347/removing-substring-from-a-string
void removeSubstring(char *s,const char *toremove)
{
  while( s=strstr(s,toremove) )
    memmove(s,s+strlen(toremove),1+strlen(s+strlen(toremove)));
}

int stringToInt(const char* inString)
{
	//integer number representation from input string
	int number;

	if(inString[0] == '-')
	{
		int sign = -1;
		//skip sign at 
		number = atoi(inString + 1);
		return sign * number;
	}
	else if(inString[0] == '+')
	{
		number = atoi(inString + 1);
		return number;
	}

	number = atoi(inString);
	return number;
}

//[time in ms] <- pixels of picture, amount of pixels, user given brightness modifier
double adjustBrightnessNormal(bitmap_pixel_hsv_t* pixels, unsigned length, float brightVal)
{
	//Start time
	clock_t tstart = clock();
	double time;

	bitmap_component_t range, brightness;
	for (int i = 0; i < length; ++i)
	{
		//HSV representation to change brightness directly
		bitmap_pixel_hsv_t* pixel = (bitmap_pixel_hsv_t*)&pixels[i];

		brightness = pixel->v;
		if (brightVal > 0)
			range = 255-brightness;
		else
			range = brightness;

		pixel->v = (bitmap_component_t)(brightness + (int)(brightVal * range));
	}

	//compute and return time in ms
	clock_t clocks = clock() - tstart;	
	time = (double)(clocks) /(CLOCKS_PER_SEC / 1000.0);
	return time;
}

//only one if statement -> TODO: change sse2 the same way 
double adjustBrightnessNormal2(bitmap_pixel_hsv_t* pixels, unsigned length, float brightVal)
{
	if(brightVal > 0)
	{
		bitmap_component_t range, brightness;

		//Start time
		clock_t tstart = clock();
		double time;

		for (int i = 0; i < length; ++i)
		{
			//HSV representation to change brightness directly
			bitmap_pixel_hsv_t* pixel = (bitmap_pixel_hsv_t*)&pixels[i];
			range = 255-brightness;	
			pixel->v = (bitmap_component_t)(brightness + (int)(brightVal * range));
		}

		//compute and return time in ms
		clock_t clocks = clock() - tstart;	
		time = (double)(clocks) /(CLOCKS_PER_SEC / 1000.0);
		return time;
	}
	else
	{
		bitmap_component_t range, brightness;

		//Start time
		clock_t tstart = clock();
		double time;

		for (int i = 0; i < length; ++i)
		{
			//HSV representation to change brightness directly
			bitmap_pixel_hsv_t* pixel = (bitmap_pixel_hsv_t*)&pixels[i];
			range = brightness;	
			pixel->v = (bitmap_component_t)(brightness + (int)(brightVal * range));
		}

		//compute and return time in ms
		clock_t clocks = clock() - tstart;	
		time = (double)(clocks) /(CLOCKS_PER_SEC / 1000.0);
		return time;
	}
}

double adjustBrightnessSSE2(bitmap_pixel_hsv_t* pixels, bitmap_component_t** values128, unsigned length, float brightVal)
{
	__m128 range, brightness;
	//SSE pointer to value array
	__m128** sse2Values = (__m128i**)(values128);

	//set SSE constants
	__m128 full = _mm_set_ps1(255.0f);
	__m128 sseBrightVal = _mm_set_ps1(brightVal);

	unsigned sse2Length = length / 4;

	//Start time
	clock_t tstart = clock();
	double time;

	//TODO
	/*
	Hier funktioniert es nicht mehr.
	Aus irgendeinem Grund sind alle value Werte der HSV Pixel beim auslesen innerhalb der Funktion 0.
	Bevor der Pointer an die Funktion übergeben wird, lassen sich alle Werte wie gewohnt auslesen.
	*/
	for (int i = 0; i < length; ++i)
	{
		//printf("i: %d\t%d\n", i, (pixels + i)->v);
	}
	return 0.0f;

	//create memory aligned array only with the value component of hsv
	for (unsigned i = 0; i < length; ++i)
	{
		values128[i] = (bitmap_component_t*)(&((pixels + i)->v));
	}

	//loop with sse intrinsics
	for (unsigned i = 0; i < sse2Length; ++i)
	{
		brightness = *(sse2Values[i]);
		//check if brightness values seem to be correct
		/*printf("bright1: %f\nbright2: %f\nbright3: %f\nbright4: %f\n\n", *((float*)&(brightness)), 
			*(((float*)&(brightness)) + 1),
			*(((float*)&(brightness)) + 2), 
			*(((float*)&(brightness)) + 3));*/
		if (brightVal > 0)
			range = _mm_sub_ps(full, brightness);
		else
			range = brightness;

		// = (brightness + (brightVal * range))
		brightness = _mm_add_ps(brightness, _mm_mul_ps(sseBrightVal, range));
	}

	//do everything out of sse range 'normally'
	bitmap_component_t brightnessNormal, rangeNormal;
	for (unsigned i = sse2Length; i < length; ++i)
	{
		//HSV representation to change brightness directly
		bitmap_pixel_hsv_t* pixel = (bitmap_pixel_hsv_t*)&pixels[i];
		
		brightnessNormal = pixel->v;
		if (brightVal > 0)
			rangeNormal = 255-brightnessNormal;
		else
			rangeNormal = brightnessNormal;

		pixel->v = (bitmap_component_t)(brightnessNormal + (int)(brightVal * rangeNormal));
	}
	
	//compute and return time in ms
	clock_t clocks = clock() - tstart;	
	time = (double)(clocks) /(CLOCKS_PER_SEC / 1000.0);
	return time;
}

double adjustBrightnessAVX2(bitmap_pixel_hsv_t* pixels, bitmap_component_t** values256, unsigned length, float brightVal)
{
	/*__m256 brightness, range;
	__m256i* avx2Values = (__m256i*)(values256);

	unsigned rest = length % 32;
	unsigned avx2Length = length / 32;

	//Start time
	clock_t tstart = clock();
	double time;
	
	
	
	//create memory aligned array only with the value component of hsv
	for (unsigned i = 0; i < length; ++i)
	{
		values256[i] = (bitmap_component_t*)(&((pixels + i)->v));
	}


	for (int i = 0; i < avx2Length; ++i)
	{
		brightness = _mm256_castsi256_ps(*(avx2Values + i));
		if (brightVal > 0)
		{
			range = _mm256_castsi256_ps(_mm256_sub_epi8(_mm256_set1_epi8(255), _mm256_castps_si256(brightness)));
		}
		else
			range = brightness;

		// = ((brightVal * range) + brightness)
		avx2Values[i] =  _mm256_castps_si256(_mm256_fmadd_ps(_mm256_castsi256_ps((_mm256_set1_epi8(brightVal))), range, brightness));
	}

	bitmap_component_t brightnessNormal, rangeNormal;
	for (int i = avx2Length; i < length; ++i)
	{
		//HSV representation to change brightness directly
		bitmap_pixel_hsv_t* pixel = (bitmap_pixel_hsv_t*)&pixels[i];
		
		brightnessNormal = pixel->v;
		if (brightVal > 0)
			rangeNormal = 255-brightnessNormal;
		else
			rangeNormal = brightnessNormal;

		pixel->v = (bitmap_component_t)(brightnessNormal + (int)(brightVal * rangeNormal));
	}
	
	//compute and return time in ms
	clock_t clocks = clock() - tstart;	
	time = (double)(clocks) /(CLOCKS_PER_SEC / 1000.0);
	return time;*/
	return 0.0f;
}


double adjustBrightnessEfficient(bitmap_pixel_hsv_t* pixels,  unsigned length, float brightVal, 
	bitmap_component_t** values128, bitmap_component_t** values256)
{
	unsigned rax, rbx, rcx, rdx;

	//Check for AVX2 support (EAX=7, ECX=0)
	__asm__ __volatile__(
		//ATT Syntax (standart) oder Intel Syntax (hier)
		".intel_syntax noprefix \n"
		//0 ins RAX Register, um CPU informationen zu bekommen
		"mov	rax, 7			\n"
		"mov	rcx, 0			\n"
		"cpuid 					\n"
		//Danach folgt wieder der ATT Syntax
		".att_syntax prefix		\n"
		//Speichert in C Variable rax
		: "=a"(rax), "=b"(rbx), "=c"(rcx), "=d"(rdx)
	);

	//EBX 5 = AVX2
	if((rbx >> 5) & 1)
	{
		return adjustBrightnessAVX2(pixels, values256, length, brightVal);
	}
	else
	{
		//Check for AVX and SSE Support (EAX=1)
		__asm__ __volatile__(
			//ATT Syntax (standart) oder Intel Syntax (hier)
			".intel_syntax noprefix \n"
			//0 ins RAX Register, um CPU informationen zu bekommen
			"mov	rax, 1			\n"
			"cpuid 					\n"
			//Danach folgt wieder der ATT Syntax
			".att_syntax prefix		\n"
			//Speichert in C Variable rax
			: "=a"(rax), "=b"(rbx), "=c"(rcx), "=d"(rdx)
		);
		
		//EDX 26 = SSE2
		if((rdx >> 26) & 1)
		{
			return adjustBrightnessSSE2(pixels, values128, length, brightVal);
		}
		//Nothing Supported -> use Normal
		else
		{
			return adjustBrightnessNormal2(pixels, length, brightVal);
		}
	}
}

void adjustBrightnessComparison(bitmap_pixel_hsv_t* pixels, unsigned length, float brightVal, 
	bitmap_component_t** values128, bitmap_component_t** values256)
{
	unsigned rax, rbx, rcx, rdx;

	printf("\n\n\n\n");

	//Check for AVX and SSE Support (EAX=1)
	__asm__ __volatile__(
		//ATT Syntax (standart) oder Intel Syntax (hier)
		".intel_syntax noprefix \n"
		//0 ins RAX Register, um CPU informationen zu bekommen
		"mov	rax, 1			\n"
		"cpuid 					\n"
		//Danach folgt wieder der ATT Syntax
		".att_syntax prefix		\n"
		//Speichert in C Variable rax
		: "=a"(rax), "=b"(rbx), "=c"(rcx), "=d"(rdx)
	);
	
	//Normal
	printf("Time Normal: %fms\n", adjustBrightnessNormal2(pixels, length, brightVal)); 		
	//EDX 26 = SSE2
	if((rdx >> 26) & 1)
	{
		printf("Time SSE2: %fms\n", adjustBrightnessSSE2(pixels, values128, length, brightVal)); 
	}

	//Check for AVX2 support (EAX=7, ECX=0)
	__asm__ __volatile__(
		//ATT Syntax (standart) oder Intel Syntax (hier)
		".intel_syntax noprefix \n"
		//0 ins RAX Register, um CPU informationen zu bekommen
		"mov	rax, 7			\n"
		"mov	rcx, 0			\n"
		"cpuid 					\n"
		//Danach folgt wieder der ATT Syntax
		".att_syntax prefix		\n"
		//Speichert in C Variable rax
		: "=a"(rax), "=b"(rbx), "=c"(rcx), "=d"(rdx)
	);

	//EBX 5 = AVX2
	if((rbx >> 5) & 1)
	{
		printf("Time AVX2: %fms\n", adjustBrightnessAVX2(pixels, values256, length, brightVal)); 
	}	
	printf("\n\n\n\n");
}

int main(int argc, char** argv)
{
	//Exit if not 3 Parameters (4 with application name) or brightness out of range
	//Parameterform: [dateiname][speicherpfad][option][helligkeit]
	if(argc != 4 || stringToInt(argv[3]) < -100 || stringToInt(argv[3]) > 100)
	{
		printf("Noetige Parameterform:\n[Dateiname][Speicherpfad][Option][Helligkeit zwischen -100 und 100]");
		return -1;
	}
	
	//If argument is changing brightness
	if(argv[2][0] == 'b')
	{
		//get value of brightness argument, is in range of [-1.0, 1.0]
		float brightVal = ((float)stringToInt(argv[3]))/100.0;

		//Buffers + width and hight of image
		bitmap_pixel_hsv_t* pixels;
		bitmap_component_t** values256, **values128;
		int widthPx, heightPx;
		//Read pixels from Bitmap 'sails.bmp' to buffer
		bitmap_error_t result = bitmapReadPixels(argv[1], 
			(bitmap_pixel_t**)&pixels,
			&widthPx,
			&heightPx,
			BITMAP_COLOR_SPACE_HSV );

		//allocate correct aligned memory for avx (256 Byte, so 32 4Bit Pixel) of value pointers from hsv
		posix_memalign((void**)&values256, 256, sizeof(bitmap_component_t*) * widthPx * heightPx);
		//allocate correct aligned memory for sse (128 Byte, so 16 4Bit Pixel) of value pointers from hsv
		posix_memalign((void**)&values128, 128, sizeof(bitmap_component_t*) * widthPx * heightPx);

		//hier funktioniert noch alles
		for (int i = 0; i < widthPx * heightPx; ++i)
		{
			//printf("i: %d\t%d\n", i, (&pixels[0] + i)->v);
		}
		//return 0;

		//manipulate picture
		adjustBrightnessComparison(&pixels[0], widthPx * heightPx, brightVal, &values128, &values256);
		//adjustBrightnessEfficient(&pixels[0], widthPx * heightPx, brightVal, &values128, &values256);
		

		//Parameters to write result
		bitmap_parameters_t parameters = 
		{
			.bottomUp = BITMAP_BOOL_TRUE,
			.widthPx = widthPx,
			.heightPx = heightPx,
			.colorDepth = BITMAP_COLOR_DEPTH_32,
			.compression = BITMAP_COMPRESSION_NONE,
			.dibHeaderFormat = BITMAP_DIB_HEADER_INFO,
			.colorSpace = BITMAP_COLOR_SPACE_HSV
		};

		//Create filename
		removeSubstring(argv[1], ".bmp");
		char fileName[150] = "";
		if(brightVal > 0)
		{
			strcpy(fileName, "_brighter.bmp");
		}
		else if(brightVal < 0)
		{
			strcpy(fileName, "_darker.bmp");
		}
		else 
		{
			strcpy(fileName, "_unchanged.bmp");
		}

	
		//Write result back to a modified file
		result = bitmapWritePixels(strcat(argv[1], fileName), 
				BITMAP_BOOL_TRUE,
				&parameters,
				(bitmap_pixel_t*)pixels);			
	}
	return 0;
}	