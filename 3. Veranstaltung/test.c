#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void writeByte(FILE* output, int byte)
{
	if(fputc(byte, output) == EOF)
	{
		printf("Writing error\n");
		exit(EXIT_FAILURE);
	}
}

void writeOutHom(FILE* output, int length, int byte)
{
	printf("%02x\n", length | 0x80);
	//writeByte(output, length | 0x80);
	printf("%02x\n", byte);
	//writeByte(output, byte);
}

void writeOutHet1(FILE* output, int length, int* buffer)
{
	//writeByte(output, length);
	printf("%d\n", length);
	for (int i = length; i > 0 ; i--)
	{
		printf("%d\n", buffer[i -1]);
		//writeByte(output, buffer[i-1]);
	}
}

void writeOutHet2(FILE* output, int length, int* buffer)
{
	printf("%d\n", length);
	for (int i = 0; i < length ; i++)
	{
		printf("%d\n", buffer[i]);
	}
}

void writeOutHet(FILE* output, int length, int* buffer)
{
	printf("%02x\n", length);
	//writeByte(output, length);
	for (int i = 0; i < length; ++i)
	{
		printf("%02x\n", buffer[i]);
		//writeByte(output, buffer[i]);
	}
}

void writeTest(FILE* output)
{
	writeByte(output, 0x2a);
	writeByte(output, 0x2a);
	writeByte(output, 0x2a);
	writeByte(output, 0x2a);

	writeByte(output, 0x1);
	writeByte(output, 0x2);
	writeByte(output, 0x3);

	writeByte(output, 0x0);
	writeByte(output, 0x0);
	writeByte(output, 0x0);
	writeByte(output, 0x0);

	writeByte(output, 0x77);
}

int main(int argc, char const *argv[])
{
	//int buffer[3] = {0x1, 0x2, 0x3};
	//writeByte(stdout, 0xA);
	//writeOutHom(stdout, 4, 0x2a);
	//writeOutHet1(stdout, 3, buffer);
	//writeOutHet(stdout, 3, buffer);
	writeTest(stdout);
	return 0;
}