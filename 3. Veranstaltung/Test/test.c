#include<stdio.h>

void test(char* input1, char** input2)
{
	printf("input1: %d\tinput2: %d\n", input1, input2);
	printf("*input: %d\n", *input1);
	for (int i = 0; i < 3; ++i)
	{
		printf("input1[i]: %c\tinput2[i]: %c\n", *(input1 + i), input2[i]);
		printf("Address: %d\n", input1 + i);
	}
}

int main(void)
{
	char test1[3] = {'a', 'b', 'c'};
	char** test1ptr = &(test1[0]);
	char** test2ptr = &test1;

	for (int i = 0; i < 3; ++i)
	{
		printf("Test[i]: %c\n", test1[i]);
		printf("Address: %d\n", &test1[i]);
	}

	printf("**: %d\t *: %d\tfirst: %d\n", &test1, test1, &(test1[0]));
	printf("test1ptr: %d\n", test1ptr);
	printf("test2ptr: %d\n", test2ptr);

	test(test1, &test1[0]);

	return 0;
}