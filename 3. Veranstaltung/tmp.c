/*Wir erzeugen einen neuen Prozess*/
   if ((pid = fork ()) < 0) {
      perror ("fork");
      exit (EXIT_FAILURE);
   }
   else if (pid > 0) {   /*Elternprozess */
      close (fd[0]);   /*Leseseite schließen */
      /* Datei auslesen (PIPE_BUF Bytes) */
      n = read (fd1, puffer, PIPE_BUF);
      /* In die Schreibseite der Pipe schreiben */
      if ((write (fd[1], puffer, n)) != n) {
         perror ("write");
         exit (EXIT_FAILURE);
      }
      /* Warten auf den Kindprozess */
      if ((waitpid (pid, NULL, 0)) < 0) {
         perror ("waitpid");
         exit (EXIT_FAILURE);
      }
   }
   else {        /*Kindprozess */
      close (fd[1]);   /*Schreibseite schließen */
      /* Leseseite der Pipe auslesen (PIPE_BUF Bytes) */
      n = read (fd[0], puffer, PIPE_BUF);
      /* Daten auf Standardausgabe schreiben */
      if ((write (STDOUT_FILENO, puffer, n)) != n) {
         perror ("write");
         exit (EXIT_FAILURE);
      }
   }