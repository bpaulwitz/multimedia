#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include "bitmap.h"

//https://stackoverflow.com/questions/4833347/removing-substring-from-a-string
void removeSubstring(char *s,const char *toremove)
{
  while( s=strstr(s,toremove) )
    memmove(s,s+strlen(toremove),1+strlen(s+strlen(toremove)));
}

int stringToInt(const char* inString)
{
	//ignorieren +/-, wird danach gemacht
	int output = atoi(inString+1);

	//falls neg. Zahl eingegeben, mit -1 multiplizieren
	if(inString[0] == '-')
		return output * -1;
	else if(inString[0] == '+')
		return output;

	printf("Fehler, kein Vorzeichen angegeben\n");
	return -1;
}

//maps to range [0, 1] and computes result 
//based on whether it gets darker, stays or gets brighter
bitmap_component_t mapping(bitmap_component_t value, int argument)
{
	//[-100, 100] -> [0, 1]
	//https://stackoverflow.com/questions/5731863/mapping-a-numeric-range-onto-another
	float newRange = 0.005*argument + 0.5;

	bitmap_component_t result;
	if(newRange < 0.5)
	{
		result = -value + newRange*(0+value);
	}
	else if(newRange == 0.5)
	{
		result = 0;
	}
	else
	{
		result = 0 + newRange*((255-value)-0);
	}
	return result;
}

int main(int argc, char** argv)
{
	//Error if not 3 Parameters (4 with application name) or brightness out of range
	//Parameterform: [dateiname][speicherpfad][option][helligkeit]
	if(argc != 4 || stringToInt(argv[3]) < -100 || stringToInt(argv[3]) > 100)
	{
		printf("Noetige Parameterform:\n[Dateiname][Speicherpfad][Option][Helligkeit zwischen -100 und 100]");
		return -1;
	}
	
	//If argument is changing brightness
	if(argv[2][0] == 'b')
	{
		//get value of brightness argument
		int brightVal = stringToInt(argv[3]);

		//Buffer + width and hight of image
		bitmap_pixel_hsv_t* pixels;
		int widthPx, heightPx;
		//Read pixels from Bitmap 'sails.bmp' to buffer
		bitmap_error_t result = bitmapReadPixels(argv[1], 
		(bitmap_pixel_t**)&pixels,
		&widthPx,
			&heightPx,
			BITMAP_COLOR_SPACE_HSV );

		//Check if funktion worked properly
		assert(result == BITMAP_ERROR_SUCCESS);
		//Print some info
		printf("Width: %d\nHeight: %d\n", widthPx, heightPx);

		//manipulate picture
		for (int i = 0; i < (widthPx * heightPx); i++)
		{
			//HSV representation to change brightness directly
			bitmap_pixel_hsv_t* pixel = (bitmap_pixel_hsv_t*)&pixels[i];
			
			pixel->v += mapping(pixel->v, brightVal);
		}

		//Parameters to write result
		bitmap_parameters_t parameters = 
		{
			.bottomUp = BITMAP_BOOL_TRUE,
			.widthPx = widthPx,
			.heightPx = heightPx,
			.colorDepth = BITMAP_COLOR_DEPTH_32,
			.compression = BITMAP_COMPRESSION_NONE,
			.dibHeaderFormat = BITMAP_DIB_HEADER_INFO,
			.colorSpace = BITMAP_COLOR_SPACE_HSV
		};

		removeSubstring(argv[1], ".bmp");

		char fileName[150] = "";
		//Filename
		if(brightVal > 0.5)
		{
			strcpy(fileName, "_brighter.bmp");
		}
		else if(brightVal < 0.5)
		{
			strcpy(fileName, "_darker.bmp");
		}
		else 
		{
			strcpy(fileName, ".bmp");
		}

	
		//Write result back to a modified file
		result = bitmapWritePixels(strcat(argv[1], fileName), 
				BITMAP_BOOL_TRUE,
				&parameters,
				(bitmap_pixel_t*)pixels);			
		}
		return 0;
	}	