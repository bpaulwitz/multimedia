#include "glCalls.h"
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <math.h>
#include "bitmap.h"
#include "objImport.h"

float X_ROT_SPEED = 0.25;
float Y_ROT_SPEED = 0.5;

void glCheck(const char* errorText){
	GLenum error = glGetError();

	if(error != GL_NO_ERROR){
		printf("GLError: %s - %d\n", errorText, error);
		exit(EXIT_FAILURE);
	}
}

char* readShaderFromFile(char* filename) {
	FILE *fp;
	long lSize;
	char *buffer;

	fp = fopen ( filename , "rb" );
	assert(fp);

	fseek( fp , 0L , SEEK_END);
	lSize = ftell( fp );
	rewind( fp );

	/* allocate memory for entire content */
	buffer = calloc( 1, lSize + 1 );
	if ( !buffer ) fclose(fp), fputs("memory alloc fails", stderr), exit(1);

	/* copy the file into the buffer */
	if ( 1 != fread( buffer , lSize, 1 , fp) )
		fclose(fp), free(buffer), fputs("entire read fails", stderr), exit(1);

	return buffer;
}

GLuint compileShader(GLenum type, const char* shaderSource, char* shaderTag){

	//shader handle
	GLuint shader;

	//create empty shader object
	shader = glCreateShader(type);

	if(shader == 0) return 0;

	//put source code into empty shader object
	glShaderSource(shader, 1, &shaderSource, NULL);

	//compile shader
	glCompileShader(shader);

	//compile status
	GLint compiled;

	//check compile status
	glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);

	if( !compiled ){

		GLint infoLen = 0;

		//get length of error message
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLen);

		if( infoLen > 1){
			char* errorMsg = malloc(sizeof(char) * infoLen);

			//get error message
			glGetShaderInfoLog(shader, infoLen, NULL, errorMsg);

			printf("Error compiling shader (%s): %s\n", shaderTag, errorMsg);

			free(errorMsg);
		}

		// delete shader object
		glDeleteShader(shader);

		return GL_FALSE;
	}

	return shader;
}

GLuint loadTexture(){
	GLuint texture;

	// Generate empty texture object
	glGenTextures(1, &texture);
	glCheck("glGenTextures");

	// ACtivate texture unit
	glActiveTexture(GL_TEXTURE0);
	glCheck("glActivateTexture(GL_TEXTURE0)");

	// Bind the texture object to the active texture unit
	glBindTexture(GL_TEXTURE_2D, texture);
	glCheck("glBindTexture(GL_TEXTURE_2D, texture");

	// Set some smpling parameters
	//wraping
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	// Minification/ magnification filter
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	// Load bitmap
	bitmap_pixel_rgb_t* pixels;
	int widthPx, heightPx;

	assert(bitmapReadPixels("logo2.bmp", (bitmap_pixel_t**)&pixels, &widthPx, &heightPx, BITMAP_COLOR_SPACE_RGB) == BITMAP_ERROR_SUCCESS);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, widthPx, heightPx, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
	glCheck("glTexImage2D");

	free(pixels);

	return texture;
}

GLboolean init(GLFWwindow* window){

	//allocate memory for user data
	UserData* userData = malloc(sizeof(UserData));
	glfwSetWindowUserPointer(window, (void*)userData);
	
	//Create the vertex shader:
	char* vertexShaderSource = readShaderFromFile("shaders/vertex.glsl");

	GLuint vertexShader = compileShader(GL_VERTEX_SHADER, vertexShaderSource, "Vertex shader");

	free(vertexShaderSource);

	//Create the fragment shader:
	char* fragmentShaderSource = readShaderFromFile("shaders/fragment.glsl");
	GLuint fragmentShader = compileShader(GL_FRAGMENT_SHADER, fragmentShaderSource, "Fragment shader");

	free(fragmentShaderSource);

	// hint to release resources of shader compiler
	glReleaseShaderCompiler();

	//program handle
	GLuint programObject;

	//create empty program object
	programObject = glCreateProgram();

	if(programObject == 0) return 0;

	// attach shaders to program object
	glAttachShader(programObject, vertexShader);
	glAttachShader(programObject, fragmentShader);

	// link shaders
	glLinkProgram(programObject);
	glCheck("glLinkProgram");

	// detach shaders from program object
	glDetachShader(programObject, vertexShader);
	glDetachShader(programObject, fragmentShader);

	// delete shader objects
	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);

	// link status
	GLint linked;

	// check link status
	glGetProgramiv(programObject, GL_LINK_STATUS, &linked);
	glCheck("glGetProgramiv");

	if( !linked ){

		GLint infoLen;

		//get length of linker error message
		glGetProgramiv(programObject, GL_INFO_LOG_LENGTH, &infoLen);

		if( infoLen > 1){
			char* errorMsg = malloc(sizeof(char) * infoLen);

			//get error message from program object
			glGetProgramInfoLog(programObject, infoLen, NULL, errorMsg);

			printf("Error linking program: %s\n", errorMsg);

			free(errorMsg);
		}

		glDeleteProgram(programObject);

		return GL_FALSE;
	}

	// use program
	glUseProgram(programObject);
	glCheck("glUseProgram");

	// store program object in context
	userData->programObject = programObject;

	// get uniform locations
	userData->angleXLoc = glGetUniformLocation(userData->programObject, "angle_x");
	glCheck("glGetUniformLocation(...) angle_x");

	userData->angleYLoc = glGetUniformLocation(userData->programObject, "angle_y");
	glCheck("glGetUniformLocation(...) angle_y");

	userData->scaleLoc = glGetUniformLocation(userData->programObject, "scale");
	glCheck("glGetUniformLocation(...) scale");

	userData->texLoc = glGetUniformLocation(userData->programObject, "tex");
	glCheck("glGetUniformLocation(...) tex");

	userData->n_loc = glGetUniformLocation(userData->programObject, "phongShader_n");
	glCheck("glGetUniformLocation(...) phongShader_n");

	userData->amb_loc = glGetUniformLocation(userData->programObject, "phongShader_c_amb");
	glCheck("glGetUniformLocation(...) phongShader_c_amb");

	userData->diff_loc = glGetUniformLocation(userData->programObject, "phongShader_c_diff");
	glCheck("glGetUniformLocation(...) phongShader_c_diff");

	userData->spec_loc = glGetUniformLocation(userData->programObject, "phongShader_c_spec");
	glCheck("glGetUniformLocation(...) phongShader_c_spec");

	// initialize the model
	userData->time = glfwGetTime();
	userData->angleX = 0;
	userData->angleY = 0;
	userData->scale = 1.0;
	userData->scaleDelta = 0.1;

	// create and bind dummy vao... it is needed in desktop OpenGL
	glGenVertexArrays(1, &(userData->vertexArrayObject));
	glCheck("glGenVertexArray");

	glBindVertexArray(userData->vertexArrayObject);
	glCheck("glBindVertexArray");

	// generate a VBO
	glGenBuffers(1, &(userData->vertexBufferObject));
	glCheck("glGenBuffers");

	glBindBuffer(GL_ARRAY_BUFFER, userData->vertexBufferObject);
	glCheck("glBindBuffer");

	//Open the obj file:
	const char* objFilePath = "teapot";

	FILE* objFile = fopen(objFilePath, "r");
	assert(objFile);

	//Count the entries:
	int vertexCount = 0;
	int texCoordsCount = 0;
	int normalCount = 0;
	int faceCount = 0;
	int mtlLibCount = 0;

	objCountEntries(objFile, &vertexCount, &texCoordsCount, &normalCount, &faceCount, &mtlLibCount);

	printf("Parsed obj file \"%s\":\n", objFilePath);
	printf("\tVertices: %d\n", vertexCount);
	printf("\tTexture coordinates: %d\n", texCoordsCount);
	printf("\tNormals: %d\n", normalCount);
	printf("\tFaces: %d\n", faceCount);
	printf("\tMaterial libraries: %d\n", mtlLibCount);

	//Rewind the file pointer:
	assert(fseek(objFile, 0, SEEK_SET) == 0);

	//Allocate the vertex array:
	int vertexCounter = 0;
	obj_vertex_entry_t* vertices = (obj_vertex_entry_t*)malloc(vertexCount * sizeof(obj_vertex_entry_t));
	assert(vertices);

	//Allocate the normal array:
	int normalCounter = 0;
	obj_normal_entry_t* normals = (obj_normal_entry_t*)malloc(normalCount * sizeof(obj_normal_entry_t));
	assert(normals);

	//Allocate the texture coordinates array:
	int textureCoordsCounter = 0;
	obj_tex_coords_entry_t* textureCoords = (obj_tex_coords_entry_t*)malloc(texCoordsCount * sizeof(obj_tex_coords_entry_t));
	assert(textureCoords);

	//Allocate vertex data array:
	int vertexDataCounter = 0;
	userData->vertexDataCount = 3 * faceCount;
	VertexData* vertexData = (VertexData*)malloc(userData->vertexDataCount * sizeof(VertexData));

	//Iterate over the entries:
	obj_entry_type_t entryType;
	obj_entry_t entry;

	while ((entryType = objGetNextEntry(objFile, &entry)) != OBJ_ENTRY_TYPE_END)
	{
		switch (entryType)
		{
		case OBJ_ENTRY_TYPE_VERTEX:

			//We have found a vertex.
			vertices[vertexCounter++] = entry.vertexEntry;
			break;

		case OBJ_ENTRY_TYPE_NORMAL:

			//We have found a normal.
			normals[normalCounter++] = entry.normalEntry;
			break;

		case OBJ_ENTRY_TYPE_TEX_COORDS:

			//We have found a texture coordinate.
			textureCoords[textureCoordsCounter++] = entry.texCoordsEntry;
			break;

		case OBJ_ENTRY_TYPE_FACE:
		{
			//We have found a face.
			//Iterate over the three vertices:
			obj_face_entry_t faceEntry = entry.faceEntry;

			for (int i = 0; i < 3; i++)
			{
				//Get the current indices:
				int vertexIndex = faceEntry.triples[i].vertexIndex;
				int normalIndex = faceEntry.triples[i].normalIndex;
				int texCoordsIndex = faceEntry.triples[i].texCoordsIndex;

				//Retrieve vertex / normal / texCoords entry.
				obj_vertex_entry_t vertexEntry = vertices[vertexIndex];
				obj_normal_entry_t normalEntry = normals[normalIndex];
				obj_tex_coords_entry_t textureCoordsEntry = textureCoords[texCoordsIndex];
				
				VertexData newVertexData = {
					.position = {
						(GLfloat)vertexEntry.x,
						(GLfloat)vertexEntry.y,
						(GLfloat)vertexEntry.z
					},
					.normal = {
						(GLfloat)normalEntry.x,
						(GLfloat)normalEntry.y,
						(GLfloat)normalEntry.z
					},
					.textureCoordinate = {
						(GLfloat)textureCoordsEntry.u,
						(GLfloat)textureCoordsEntry.v
					},
					.color = {
						0x80,0x80,0x80
					}
				};
				//Build vertex data from them.
				vertexData[vertexDataCounter++] = newVertexData;
			}

			break;
		}
		}
	}

	//Buffer the data:
	glBufferData(GL_ARRAY_BUFFER, userData->vertexDataCount * sizeof(VertexData), (GLvoid*)vertexData, GL_STATIC_DRAW);
	glCheck("glBufferData(...)");

	//Release stuff:
	free(vertices);
	free(vertexData);

	fclose(objFile);

	// upload data to gpu
	//glBufferData(GL_ARRAY_BUFFER, sizeof(vertexData), (const GLvoid*) vertexData, GL_STATIC_DRAW);
	//glCheck("glBufferData");

	//attribute index, component count, normalize, stride, pointer
	//enable vertex attribute
	glVertexAttribPointer(ATTRIB_POSITION, 3, GL_FLOAT, GL_FALSE, sizeof(VertexData), (GLvoid*)offsetof(VertexData, position));
	glCheck("glVertexAttribPointer - ATTRIB_POSITION");
	glEnableVertexAttribArray(ATTRIB_POSITION);
	glCheck("glEnableVertexAttribArray - ATTRIB_POSITION");

	//attribute index, component count, normalize, stride, pointer
	//enable vertex attribute
	glVertexAttribPointer(ATTRIB_NORMAL, 3, GL_FLOAT, GL_FALSE, sizeof(VertexData), (GLvoid*)offsetof(VertexData, normal));
	glCheck("glVertexAttribPointer - ATTRIB_NORMAL");
	glEnableVertexAttribArray(ATTRIB_NORMAL);
	glCheck("glEnableVertexAttribArray - ATTRIB_NORMAL");

	//attribute index, component count, normalize, stride, pointer
	//enable vertex attribute
	glVertexAttribPointer(ATTRIB_TEX_COORD, 2, GL_FLOAT, GL_FALSE, sizeof(VertexData), (GLvoid*)offsetof(VertexData, textureCoordinate));
	glCheck("glVertexAttribPointer - ATTRIB_TEX_COORD");
	glEnableVertexAttribArray(ATTRIB_TEX_COORD);
	glCheck("glEnableVertexAttribArray - ATTRIB_TEX_COORD");

	//attribute index, component count, normalize, stride, pointer
	//enable vertex attribute
	glVertexAttribPointer(ATTRIB_COLOR, 3, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(VertexData), (GLvoid*)offsetof(VertexData, color));
	glCheck("glVertexAttribPointer - ATTRIB_COLOR");
	glEnableVertexAttribArray(ATTRIB_COLOR);
	glCheck("glEnableVertexAttribArray - ATTRIB_COLOR");

	// load a texture
	userData->tex = loadTexture();

	// Assign the texture unit
	glUniform1i(userData->texLoc, 0);
	glCheck("glUniform1i(userData->texLoc, 0)");

	// get the window size
	int width, height;
	glfwGetFramebufferSize(window, &width, &height);

	// set viewport
	glViewport(0, 0, width, height);
	glCheck("glViewport");

	// define clear for color buffer with black color
	glClearColor(0.2f,0.2f,0.2f,0.0f);
	glCheck("glClearColor");

	// enable Depth test
	glEnable(GL_DEPTH_TEST);
	glCheck("glEnable(GL_DEPTH_TEST)");

	// enable Backface culling
	glEnable(GL_CULL_FACE);
	glCheck("glEnable(GL_CULLING");

	printf("\n\nChange phong shading constants with the keys n, a, d and s for 'shininess', ambient, diffuse and specular lighting constants\n");
	printf("Phong shading constants:\n\tn: %d\n\tAmbient: %f\n\tDiffuse: %f\n\tSpecular: %f\n\n", 
    		userData->phong_n, userData->phong_amb, userData->phong_diff, userData->phong_spec);

	return GL_TRUE;
}

void update(GLFWwindow* window){
	UserData* userData = (UserData*) glfwGetWindowUserPointer(window);

	// get time delta and update
	double newTime = glfwGetTime();
	double timeDelta = newTime - userData->time;

	userData->time = newTime;

	// set angle X
	userData->angleX = fmod(userData->angleX + (X_ROT_SPEED * timeDelta), 2 * M_PI);

	// set angle Y
	userData->angleY = fmod(userData->angleY + (Y_ROT_SPEED * timeDelta), 2 * M_PI);

	// set scale
	userData->scale += timeDelta * userData->scaleDelta;

	if (userData->scale < 0.25)
	{
		userData->scale = 0.25;
		userData->scaleDelta = 0.1;
	}
	else if (userData->scale > 5.0)
	{
		userData->scale = 5.0;
		userData->scaleDelta = -0.1;
	}

	userData->scale = 1.0;

	// update uniforms
	glUniform1f(userData->angleXLoc, userData->angleX);
	glCheck("glUniform1f angleX");

	glUniform1f(userData->angleYLoc, userData->angleY);
	glCheck("glUniform1f angleY");

	glUniform1f(userData->scaleLoc, userData->scale);
	glCheck("glUniform1f scale");

	glUniform1i(userData->n_loc, userData->phong_n);
	glCheck("glUniform1f n");

	glUniform1f(userData->amb_loc, userData->phong_amb);
	glCheck("glUniform1f ambient");

	glUniform1f(userData->diff_loc, userData->phong_diff);
	glCheck("glUniform1f diffuse");

	glUniform1f(userData->spec_loc, userData->phong_spec);
	glCheck("glUniform1f specular");
}

void draw (GLFWwindow* window){
	UserData* userData = (UserData*) glfwGetWindowUserPointer(window);

	// clear color buffer
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// draw stuff
	// primitive type, start index in array, number of elements to render
	glDrawArrays(GL_TRIANGLES, 0, userData->vertexDataCount);
	glCheck("glDrawArrays");

    glfwSwapBuffers(window);
}

void teardown(GLFWwindow* window){
	UserData* userData = (UserData*) glfwGetWindowUserPointer(window);

	glDeleteBuffers(1, &(userData->vertexBufferObject));
	glCheck("glDeleteBuffers");

	glDeleteVertexArrays(1, &(userData->vertexArrayObject));
	glCheck("glDeleteVertexArrays");

	free(userData);
}

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods){
	UserData* userData = (UserData*) glfwGetWindowUserPointer(window);
	if ((key == GLFW_KEY_N && action == GLFW_PRESS) || (key == GLFW_KEY_N && action == GLFW_REPEAT))
    {
    	if(userData->phong_n <= 99)
    		userData->phong_n += 1;
    	else
    		userData->phong_n = 0;
    	printf("n: %d\n", userData->phong_n);
    }

    if ((key == GLFW_KEY_A && action == GLFW_PRESS) || (key == GLFW_KEY_A && action == GLFW_REPEAT))
    {
    	if(userData->phong_amb <= 0.95f)
    		userData->phong_amb += 0.005f;
    	else
    		userData->phong_amb = 0.0f;
    	printf("ambient: %f\n", userData->phong_amb);
    }

    if ((key == GLFW_KEY_D && action == GLFW_PRESS) || (key == GLFW_KEY_D && action == GLFW_REPEAT))
    {
    	if(userData->phong_diff <= 0.95f)
    		userData->phong_diff += 0.005f;
    	else
    		userData->phong_diff = 0.0f;
    	printf("diffuse: %f\n", userData->phong_diff);
    }

    if ((key == GLFW_KEY_S && action == GLFW_PRESS) || (key == GLFW_KEY_S && action == GLFW_REPEAT))
    {
    	if(userData->phong_spec <= 0.95f)
    		userData->phong_spec += 0.005f;
    	else
    		userData->phong_spec = 0.0f;
    	printf("specular: %f\n", userData->phong_spec);
    }

    if (key == GLFW_KEY_ENTER && action == GLFW_PRESS)
    {
    	printf("Phong shading constants:\n\tn: %d\n\tAmbient: %f\n\tDiffuse: %f\n\tSpecular: %f\n\n", 
    		userData->phong_n, userData->phong_amb, userData->phong_diff, userData->phong_spec);
    }

    if (key == GLFW_KEY_BACKSPACE && action == GLFW_PRESS)
    {
    	userData->phong_n = 0;
    	userData->phong_amb = 0.0f;
    	userData->phong_diff = 0.0f;
    	userData->phong_spec = 0.0f;
    	X_ROT_SPEED = 0.25;
    	Y_ROT_SPEED = 0.5;

    	printf("Phong shading constants:\n\tn: %d\n\tAmbient: %f\n\tDiffuse: %f\n\tSpecular: %f\n\tX rotation speed: %f\n\tY rotation speed: %f\n\n", 
    		userData->phong_n, userData->phong_amb, userData->phong_diff, userData->phong_spec, X_ROT_SPEED, Y_ROT_SPEED);
    }

    if ((key == GLFW_KEY_LEFT && action == GLFW_PRESS) || (key == GLFW_KEY_LEFT && action == GLFW_REPEAT))
    {
    	Y_ROT_SPEED -= 0.25f;
       	printf("y rotation speed: %f\n", Y_ROT_SPEED);

    }

    if ((key == GLFW_KEY_RIGHT && action == GLFW_PRESS) || (key == GLFW_KEY_RIGHT && action == GLFW_REPEAT))
    {
    	Y_ROT_SPEED +=0.25f;
    	printf("y rotation speed: %f\n", Y_ROT_SPEED);

    }

    if ((key == GLFW_KEY_UP && action == GLFW_PRESS) || (key == GLFW_KEY_UP && action == GLFW_REPEAT))
    {
    	X_ROT_SPEED -=0.25f;
    	printf("x rotation speed: %f\n", X_ROT_SPEED);

    }

    if ((key == GLFW_KEY_DOWN && action == GLFW_PRESS) || (key == GLFW_KEY_DOWN && action == GLFW_REPEAT))
    {
    	X_ROT_SPEED +=0.25f;
    	printf("x rotation speed: %f\n", X_ROT_SPEED);

    }
}

void error_callback(int error, const char* description){
    fprintf(stderr, "Error: %s\n", description);
}