#include "objImport.h"

#include <stdlib.h>
#include <string.h>

#define OBJ_MAX_LINE_LENGTH 4096

//Private: Does a string start with a given prefix?
int objStartsWith(char* str, char* prefix)
{
	size_t lengthStr = strlen(str);
	size_t lengthPrefix = strlen(prefix);

	return (lengthStr < lengthPrefix) ? 0 : (strncmp(str, prefix, lengthPrefix) == 0);
}

//Private Skip spaces at the beginning of a line:
char* skipSpaces(char* str)
{
	while (*str == ' ')
		str++;

	return str;
}

void objCountEntries(FILE* objFile, int* vertexCount, int* texCoordsCount, int* normalCount, int* faceCount, int* mtlLibCount)
{
	//Set the counters to zero:
	*vertexCount = 0;
	*texCoordsCount = 0;
	*normalCount = 0;
	*faceCount = 0;
	*mtlLibCount = 0;

	//Read lines into this array:
	char line[OBJ_MAX_LINE_LENGTH];

	//Read line by line:
	while (fgets(line, OBJ_MAX_LINE_LENGTH, objFile))
	{
		//Skip potential spaces:
		char* currLine = skipSpaces(line);

		if (objStartsWith(currLine, "v "))
			(*vertexCount)++;
		else if (objStartsWith(currLine, "vt "))
			(*texCoordsCount)++;
		else if (objStartsWith(currLine, "vn "))
			(*normalCount)++;
		else if (objStartsWith(currLine, "f "))
			(*faceCount)++;
		else if (objStartsWith(currLine, "mtllib "))
			(*mtlLibCount)++;
	}
}

obj_entry_type_t objGetNextEntry(FILE* objFile, obj_entry_t* entry)
{
	//Track the indices of the next elements we receive:
	int nextVertexIndex = 0;
	int nextTexCoordsIndex = 0;
	int nextNormalIndex = 0;

	//Read lines into this array:
	char line[OBJ_MAX_LINE_LENGTH];

	//Read line by line:
	while (fgets(line, OBJ_MAX_LINE_LENGTH, objFile))
	{
		//Skip potential spaces:
		char* currLine = skipSpaces(line);

		if (objStartsWith(currLine, "v "))
		{
			currLine++;

			//Parse the vertex:
			entry->vertexEntry.x = strtod(currLine, &currLine);
			entry->vertexEntry.y = strtod(currLine, &currLine);
			entry->vertexEntry.z = strtod(currLine, &currLine);
			//TODO:
			entry->vertexEntry.w = -1;

			nextVertexIndex++;

			return OBJ_ENTRY_TYPE_VERTEX;
		}
		else if (objStartsWith(currLine, "vt"))
		{
			currLine += 2;

			//Parse the vertex:
			entry->texCoordsEntry.u = strtod(currLine, &currLine);
			entry->texCoordsEntry.v = strtod(currLine, &currLine);

			nextTexCoordsIndex++;

			return OBJ_ENTRY_TYPE_TEX_COORDS;				
		}
		else if (objStartsWith(currLine, "vn "))
		{
			currLine += 2;

			//Parse the vertex:
			entry->normalEntry.x = strtod(currLine, &currLine);
			entry->normalEntry.y = strtod(currLine, &currLine);
			entry->normalEntry.z = strtod(currLine, &currLine);

			nextNormalIndex++;

			return OBJ_ENTRY_TYPE_NORMAL;			
		}
		else if (objStartsWith(currLine, "f "))
		{
			currLine++;

			//TODO: Quads.
			for (int i = 0; i < 3; i++)
			{
				entry->faceEntry.triples[i].texCoordsIndex = -1;
				entry->faceEntry.triples[i].normalIndex = -1;

				//Vertex index:
				entry->faceEntry.triples[i].vertexIndex = strtol(currLine, &currLine, 10) - 1;

				if (entry->faceEntry.triples[i].vertexIndex < 0)
					entry->faceEntry.triples[i].vertexIndex += nextVertexIndex + 1;

				currLine = skipSpaces(currLine);

				if (*currLine != '/')
					continue;

				currLine = skipSpaces(currLine + 1);

				//Texture coordinates index:
				if (*currLine != '/')
				{
					entry->faceEntry.triples[i].texCoordsIndex = strtol(currLine, &currLine, 10) - 1;

					if (entry->faceEntry.triples[i].texCoordsIndex < 0)
						entry->faceEntry.triples[i].texCoordsIndex += nextTexCoordsIndex + 1;

					currLine = skipSpaces(currLine);
				}

				currLine = skipSpaces(currLine + 1);

				//Normal index:
				entry->faceEntry.triples[i].normalIndex = strtol(currLine, &currLine, 10) - 1;

				if (entry->faceEntry.triples[i].normalIndex < 0)
					entry->faceEntry.triples[i].normalIndex += nextNormalIndex + 1;
			}

			return OBJ_ENTRY_TYPE_FACE;
		}
	}

	//No line left.
	return OBJ_ENTRY_TYPE_END;		
}
