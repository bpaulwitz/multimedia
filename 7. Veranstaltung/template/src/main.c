#include <string.h>
#include <stdio.h>
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include "glCalls.h"

int main ( int argc, char *argv[] )
{
	// this is our window, it also holds the OpenGL context
    GLFWwindow* window;

    // register error callback
    glfwSetErrorCallback(error_callback);

    // try init glfw
    if (!glfwInit()){
        return -1;
    }

    // we want at least a 4.1 context
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);

    // enable forward-compability and core profile
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    // define depth/stencil buffer
    glfwWindowHint(GLFW_DEPTH_BITS, 24);
    glfwWindowHint(GLFW_STENCIL_BITS, 0);

    // create a window
    window = glfwCreateWindow(1024, 768, "Object Viewer", NULL, NULL);

    // check window
    if (!window){
        glfwTerminate();
        return -1;
    }

    // register key callback
    glfwSetKeyCallback(window, key_callback);

    // makes the context of our window current aka this thread is now OpenGL thread
    glfwMakeContextCurrent(window);

    // loads pointers to OpenGL functions/extensions at runtime
    gladLoadGLLoader((GLADloadproc) glfwGetProcAddress);

    // draw a new image every frame
    glfwSwapInterval(1);

    // init OpenGL stuff
    if (!init(window)){
		glfwTerminate();
        return -1;
    }

    // our render loop
    while (!glfwWindowShouldClose(window)){
        update(window);
        draw(window);
        glfwPollEvents();
    }

    teardown(window);

    glfwDestroyWindow(window);
    glfwTerminate();

	return 0;
}

















