#version 410
layout(location = 0) in vec4 vPosition;
layout(location = 1) in vec3 vNormal;
layout(location = 2) in vec2 vTexCoords;
layout(location = 3) in vec4 vColor;

//flat out vec4 fColor;
out vec4 fColor;
out vec2 fTexCoords;

//Uniforms:
uniform float angle_x;
uniform float angle_y;
uniform float scale;

void main()
{
	fColor = vColor;
	fTexCoords = vTexCoords;
	
	float near = 1.0;
	float far = 12.0;
	float left = -1.0;
	float right = 1.0;
	float top = 1.0;
	float bottom = -1.0;
	
	mat4 frustum = mat4(
		2.0 * near / (right - left),		0.0,								 0.0,								 0.0,
		0.0,								2.0 * near / (top - bottom),		 0.0,								 0.0,
		(left + right) / (right - left),	(top + bottom) / (top - bottom),	-(far + near) / (far - near),		-1.0,
		0.0,								0.0,								-2.0 * near * far / (far - near),	 0.0
	);
	
	mat4 rotx = mat4(
		1.0,			 0.0,			0.0,			0.0,
		0.0,			 cos(angle_x),	sin(angle_x),	0.0,
		0.0,			-sin(angle_x),	cos(angle_x),	0.0,
		0.0,			 0.0,			0.0,			1.0
	);
	
	mat4 roty = mat4(
		cos(angle_y),	0.0,			-sin(angle_y),	0.0,
		0.0,			1.0,			 0.0,			0.0,
		sin(angle_y),	0.0,			 cos(angle_y),	0.0,
		0.0,			0.0,			 0.0,			1.0
	);

	mat4 scaling = mat4(
		scale,	0.0,	0.0,	0.0,
		0.0,	scale,	0.0,	0.0,
		0.0,	0.0,	scale,	0.0,
		0.0,	0.0,	0.0,	1.0
	);
	
	vec4 trans = vec4(0.0, -1.0, -7.0, 1.0);

	vec4 pos = ((rotx * roty * scaling * vPosition) + trans);

	//Conververt normal into eye space and normalize:
	vec3 norm = normalize((rotx * roty * vec4(vNormal, 0.0)).xyz);

	//Direction from light source vertex:
	vec3 lightSource = vec3(0.0, 0.0, -10.0);
	vec3 lightVertexVec = normalize(pos.xyz - lightSource);

	//Calculate the agnle between light source and vertex
	float lightWeight = abs(dot(norm, lightVertexVec));

	//Apply the lights weight to our vertex Color
	fColor = max(0.1, lightWeight) * vColor;
	
	gl_Position = frustum * pos;
}
