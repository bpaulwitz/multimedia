#ifndef GLCALLS_H
#define GLCALLS_H

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#define ATTRIB_POSITION 0
#define ATTRIB_NORMAL 1
#define ATTRIB_TEX_COORD 2
#define ATTRIB_COLOR 3

typedef struct{
	GLuint programObject;
	GLuint vertexBufferObject;
	GLuint vertexArrayObject;
	GLuint vertexDataCount;
	double time;
	GLint angleXLoc;
	GLint angleYLoc;
	GLint scaleLoc;
	GLint texLoc;
	GLuint tex;
	GLfloat angleX;
	GLfloat angleY;
	GLfloat scale;
	GLfloat scaleDelta;
} UserData;

typedef struct
{
	GLfloat position[3];
	GLfloat normal[3];
	GLfloat	textureCoordinate[2];
	GLubyte color[3];
} VertexData;

GLboolean init(GLFWwindow* window);
void update(GLFWwindow* window);
void draw(GLFWwindow* window);
void teardown(GLFWwindow* window);
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods);
void error_callback(int error, const char* description);
#endif