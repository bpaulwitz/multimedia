#ifndef _OBJ_IMPORT_H_
#define _OBJ_IMPORT_H_

#include <stdio.h>

typedef int obj_entry_type_t;

#define OBJ_ENTRY_TYPE_END 0
#define OBJ_ENTRY_TYPE_VERTEX 1
#define OBJ_ENTRY_TYPE_TEX_COORDS 2
#define OBJ_ENTRY_TYPE_NORMAL 3
#define OBJ_ENTRY_TYPE_FACE 4
#define OBJ_ENTRY_TYPE_MTL_IMPORT 5
#define OBJ_ENTRY_TYPE_MTL_USE 6

//An obj vertex entry:
typedef struct _obj_vertex_entry_t_
{
	double x;
	double y;
	double z;

	//Negative if not present:
	double w;
} obj_vertex_entry_t;

//An obj texture coordinates entry:
typedef struct _obj_tex_coords_entry_t_
{
	double u;
	double v;
} obj_tex_coords_entry_t;

//An obj normal entry:
typedef struct _obj_normal_entry_t_
{
	double x;
	double y;
	double z;
} obj_normal_entry_t;

//An obj face index triple:
typedef struct _obj_face_index_triple_t_
{
	//Missing indices are negative.
	int vertexIndex;
	int texCoordsIndex;
	int normalIndex;
} obj_face_index_triple_t;

//An obj face entry:
typedef struct _obj_face_entry_t_
{
	obj_face_index_triple_t triples[3];
} obj_face_entry_t;

//An obj mtl entry:
typedef struct _obj_mtl_entry_t_
{
	char mtlName[2048];
} obj_mtl_entry_t;

//An obj entry:
typedef union _obj_entry_t_
{
	obj_vertex_entry_t vertexEntry;
	obj_tex_coords_entry_t texCoordsEntry;
	obj_normal_entry_t normalEntry;
	obj_face_entry_t faceEntry;
	obj_mtl_entry_t mtlEntry;
} obj_entry_t;

//Scan through an open obj file and count the different entries:
void objCountEntries(FILE* objFile, int* vertexCount, int* texCoordsCount, int* normalCount, int* faceCount, int* mtlLibCount);

//Parse one line of obj content.
obj_entry_type_t objGetNextEntry(FILE* objFile, obj_entry_t* entry);

#endif
















