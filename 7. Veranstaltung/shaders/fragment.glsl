#version 330

precision mediump float;       

out vec4 outColor;

in vec2 fTexCoords;
in lowp vec4 fColor;

uniform sampler2D tex;           

void main()                    
{                              
    outColor = texture(tex, fTexCoords) * fColor;
	//outColor = vec4( vec3(1.0 - gl_FragCoord.z), 1.0);      

}
